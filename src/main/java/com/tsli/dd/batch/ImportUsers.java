package com.tsli.dd.batch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import com.tsli.dd.batch.service.UsersService;

@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.tsli.dd" })
public class ImportUsers extends AbstractBatch {

	@Autowired
	UsersService usersService;

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ImportUsers.class, args);
	}

	@Override
	public void initail(String... args) throws Exception {

	}

	@Override
	public void start(String... args) throws Exception {
		setArgruments(args);
		usersService.importUser();
	}
}
