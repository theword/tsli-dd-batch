package com.tsli.dd.batch.constants;


public class InqueryConstants {
	public static final String T1 				= "IMPORT_AGENT_PERFORMANCE";
	public static final String T2 				= "IMPORT_AGENT_STRUCTURE";
	public static final String T3 				= "IMPORT_AGENT_RANKING_PERFORMANCE";
	public static final String T4 				= "IMPORT_AGENT_KPIMAPPING";
	public static final String T5 				= "IMPORT_AGENT_TARGET";
	
	public static class BatchType{
		public static final String END_DAY		= "E";
		public static final String DAILY		= "D";
	}
	
	public static class BatchSatus {
		public static final Integer DOING		= 1;
		public static final Integer DONE		= 2;
		public static final Integer ERROR		= 3;
	}
	
	
	public static class AgentLevel {
		public static final String ZONE			= "ZONE";
	}
	
	public static class UserType {
		public static final String USER			= "U";
		public static final String PRODUCER		= "P";
	}
}
