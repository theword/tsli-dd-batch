package com.tsli.dd.batch.service;

import java.util.List;

import com.tsli.dd.batch.entity.DdTAgentPerformance;
import com.tsli.dd.batch.entity.DdTAgentRankingPerformance;
import com.tsli.dd.batch.entity.DdTAgentStructure;
import com.tsli.dd.batch.entity.DdTKpiMapping;
import com.tsli.dd.batch.entity.DdTKpiTarget;
import com.tsli.dd.batch.model.AgentPerformance;

public interface TsliDDService {
	public String saveAgentPerformnaceModel(List<DdTAgentPerformance>agentPerformance,String calYm);

	public String saveKpiMapping(List<DdTKpiMapping> kpiMappingModel);

	public String saveKpiTarget(List<DdTKpiTarget> kpiMappingModel);

	public String saveAgentRankingPerformance(List<DdTAgentRankingPerformance> aAgentRankingPerformanceModel);

	public String saveAgentStructure(List<DdTAgentStructure> agentStructure);
}
