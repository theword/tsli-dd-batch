package com.tsli.dd.batch.service.impl;

import java.io.NotActiveException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.tsli.dd.batch.constants.InqueryConstants.AgentLevel;
import com.tsli.dd.batch.constants.InqueryConstants.UserType;
import com.tsli.dd.batch.entity.DdTAgentStructure;
import com.tsli.dd.batch.entity.StgImportUsers;
import com.tsli.dd.batch.entity.Users;
import com.tsli.dd.batch.repository.StglmportUserRepository;
import com.tsli.dd.batch.repository.TsliDDAgentStructureRepository;
import com.tsli.dd.batch.repository.TsliDDUserRepository;
import com.tsli.dd.batch.service.LockEngineLicenseService;
import com.tsli.dd.batch.service.UsersService;
import com.tsli.dd.batch.util.CalendarHelper;

@Service
public class UsersServiceImpl implements UsersService{
	protected Logger log = LogManager.getLogger(getClass());

	
	@Value("${lockengine.license.key}")
	private String keyLicense;
	
	private Integer maxLicense;
	
	@Autowired
	private EntityManager entityManager;

	@Autowired
	private TsliDDUserRepository tsliDDUserRepository;

	@Autowired
	private StglmportUserRepository stglmportUserRepository;

	@Autowired
	private TsliDDAgentStructureRepository  agentStructureRepository;
	
	@PostConstruct
	public void init() {
		maxLicense = LockEngineLicenseService.decodeLicense(keyLicense);
	}

	@SuppressWarnings("unchecked")
	public List<StgImportUsers> getStgUsers() {
		String sql = "select * from DATAHUB_PROD.STG_IMPORT_USERS where STATUS_TO_LOCKENGINE is null " ;
		Query query = entityManager.createNativeQuery(sql, StgImportUsers.class);
		return (List<StgImportUsers>) query.getResultList();
	}


	public void importUser() throws Exception {
		List<StgImportUsers> stgImportUsersemodel = new ArrayList<StgImportUsers>();
		stgImportUsersemodel = getStgUsers();
		saveUsers(stgImportUsersemodel);
	}
	
	
	public List<Users> saveUsers(List<StgImportUsers>stgUsers) {

		Integer licenseActive = 0;

		tsliDDUserRepository.deleteAll();
		
		for(StgImportUsers model:stgUsers) {
			try {
				
				log.info("StgImportUsers:"+model.getUserName());
		
				String license = LockEngineLicenseService.encode(model.getUserName());
				if( 'A' == model.getStatus()) {
					licenseActive++;
				}else {
					licenseActive--;
				}

				if(  maxLicense < licenseActive ) {
					throw new NotActiveException("License active '"+ licenseActive + "'. It is full.");
				}
				
				Users usersToInsert = new Users();
				usersToInsert.setUsername(model.getUserName());
				usersToInsert.setUserId(model.getUserId());
				usersToInsert.setUserType(model.getUserType());
				usersToInsert.setName(model.getName());
				usersToInsert.setSurname(model.getSurname());
				usersToInsert.setCreateDate(CalendarHelper.getCurrentDateTime().getTime());
				usersToInsert.setCreateBy("1");
				usersToInsert.setUpdateBy("1");
				usersToInsert.setUpdateDate(CalendarHelper.getCurrentDateTime().getTime());
				usersToInsert.setIsDelete('F');
				usersToInsert.setStatus(model.getStatus());
				usersToInsert.setPassword(license);
				tsliDDUserRepository.save(usersToInsert);
				
				/*
				 * Mock up user to agent structure 
				 */
				if( UserType.USER.equals(model.getUserType()) ) {
					mockupAgentStructure(usersToInsert);
				}
				
				stglmportUserRepository.setStatusToLockengin(model.getUserName(), "Y",CalendarHelper.getCurrentDateTime().getTime(), null);
			} catch (NotActiveException e) {
				log.error( e.getMessage() );
				stglmportUserRepository.setStatusToLockengin(model.getUserName(), "L",CalendarHelper.getCurrentDateTime().getTime(), e.getMessage());
			}catch (Exception e) {
				log.error( e );
				stglmportUserRepository.setStatusToLockengin(model.getUserName(), "N",CalendarHelper.getCurrentDateTime().getTime(), (""+ e.getMessage()).substring(0,e.getMessage().length() >1999?1999: e.getMessage().length()));
			}
		}
		return null;
	}

	@Transactional
	public DdTAgentStructure mockupAgentStructure( Users user ) throws Exception {
		DdTAgentStructure structure = new DdTAgentStructure();
		structure.setAgentCode( user.getUsername() );
		structure.setThaiName( user.getName() );
		structure.setThaiSurname( user.getSurname() );
		structure.setAgentLevel(AgentLevel.ZONE);
		structure.setThaiFullName(String.format("%s %s", user.getName(), user.getSurname()));
		return agentStructureRepository.save(structure);
	}

}
