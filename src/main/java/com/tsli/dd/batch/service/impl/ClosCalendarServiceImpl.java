package com.tsli.dd.batch.service.impl;


import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tsli.dd.batch.constants.InqueryConstants.BatchType;
import com.tsli.dd.batch.entity.DdTClosCalendar;
import com.tsli.dd.batch.entity.DdTKpiTarget;
import com.tsli.dd.batch.entity.StgScheduleLog;
import com.tsli.dd.batch.repository.StgScheduleLogRepository;
import com.tsli.dd.batch.repository.TsliDDClosCalendarRepository;
import com.tsli.dd.batch.service.ClosCalendarService;
import com.tsli.dd.batch.util.ObjectUtils;

@Service
public class ClosCalendarServiceImpl implements ClosCalendarService {

	@Autowired
	private EntityManager entityManager;
	@Autowired
	private TsliDDClosCalendarRepository tsliDDClosCalendarRepository;
	@Autowired
	private StgScheduleLogRepository stgScheduleLogRepository;
	
	@Transactional
	public DdTClosCalendar getNearestClosYm() {
		return tsliDDClosCalendarRepository.getNearestClosYm().get(0);
	}

	@Transactional
	public DdTClosCalendar getDdTClosCalendarByimportDate(String importDate) {
		String sql = " select * From DATAHUB_PROD.DD_T_CLOS_CALENDAR where :importDate between UW_STR_YMD and UW_END_YMD or :importDate = END_CLOSING_YMD ";
		Query query = entityManager.createNativeQuery(sql, DdTClosCalendar.class);
		if(StringUtils.contains(sql, ":importDate")){
			query.setParameter("importDate", importDate);
		}	
		return (DdTClosCalendar) query.getSingleResult();
	}
	
	@Transactional
	public List<DdTClosCalendar> getListDdTClosCalendarByClosYM(String closYM) {
		String sql = " select * From DATAHUB_PROD.DD_T_CLOS_CALENDAR where CLOS_YM =:closYM ";
		Query query = entityManager.createNativeQuery(sql, DdTClosCalendar.class);
		if(StringUtils.contains(sql, ":closYM")){
			query.setParameter("closYM", closYM);
		}
		List<DdTClosCalendar> listModel = 	ObjectUtils.toObject(query.getResultList(), DdTClosCalendar.class );
		return listModel;
	}
	
	@Transactional
	public List<DdTClosCalendar> getListDdTClosCalendarByimportDate(String importDate) {
		String sql = " select * From DATAHUB_PROD.DD_T_CLOS_CALENDAR where :importDate between UW_STR_YMD and UW_END_YMD or :importDate = END_CLOSING_YMD ";
		Query query = entityManager.createNativeQuery(sql, DdTClosCalendar.class);
		if(StringUtils.contains(sql, ":importDate")){
			query.setParameter("importDate", importDate);
		}
		List<DdTClosCalendar> listModel = 	ObjectUtils.toObject(query.getResultList(), DdTClosCalendar.class );
		return listModel;
	}

	@Transactional
	public List<DdTClosCalendar> getListDdTClosCalendarByimportDate() {
		String sql = " select * From DATAHUB_PROD.DD_T_CLOS_CALENDAR where FLAG_END_CLOSING_YMD ='F' and CLOS_YM <= (select CLOS_YM From DATAHUB_PROD.DD_T_CLOS_CALENDAR where to_char(sysdate,'yyyymmdd') between UW_STR_YMD and UW_END_YMD ) ";
		Query query = entityManager.createNativeQuery(sql, DdTClosCalendar.class);
		List<DdTClosCalendar> listModel = 	ObjectUtils.toObject(query.getResultList(), DdTClosCalendar.class );
		return listModel;
	}
	
	@Transactional
	public DdTClosCalendar getddTClosCalendar(String importDate) {
		DdTClosCalendar ddTClosCalendar = null;
		if (importDate != null) {
			ddTClosCalendar = getDdTClosCalendarByimportDate(importDate);
		} else {
			ddTClosCalendar = getNearestClosYm();
		}
		return ddTClosCalendar;
	}

	public StgScheduleLog getBatchEndDay() {
		StgScheduleLog stgScheduleLog = stgScheduleLogRepository.findByBatchType(BatchType.END_DAY.charAt(0));
		return stgScheduleLog;
	}

	public StgScheduleLog getBatchDaily() {
		StgScheduleLog stgScheduleLog = stgScheduleLogRepository.findByBatchType(BatchType.DAILY.charAt(0));
		return stgScheduleLog;
	}

	@Modifying
	public StgScheduleLog updateBatchStatus(Date startDate, Date endDate, String msg, Integer status, String batchType) {
		StgScheduleLog stgScheduleLog = stgScheduleLogRepository.findByBatchType(batchType.charAt(0));
		stgScheduleLog.setB3StartDate(startDate);
		stgScheduleLog.setB3EndDate(endDate);
		stgScheduleLog.setB3Status(status);
		stgScheduleLog.setRemark(msg);
		stgScheduleLogRepository.saveAndFlush(stgScheduleLog);
		return stgScheduleLog;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	public void importClosCalendar() {
		String sql = " select * From DATAHUB_PROD.DD_T_CLOS_CALENDAR ";
		Query query = this.entityManager.createNativeQuery(sql).unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(
						Transformers.ALIAS_TO_ENTITY_MAP
						);
		List<Map<String, Object>> data = query.getResultList();
		List<DdTClosCalendar> lists = ObjectUtils.toObject(data, DdTClosCalendar.class );
		
		tsliDDClosCalendarRepository.deleteAll();
		
		for(DdTClosCalendar entity : lists) {
			tsliDDClosCalendarRepository.saveAndFlush(entity);
		}
		
	}
	
	
	
	
}
