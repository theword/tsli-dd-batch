package com.tsli.dd.batch.service;

import java.util.List;

import com.tsli.dd.batch.entity.StgImportUsers;
import com.tsli.dd.batch.entity.Users;

public interface UsersService {

	
	public void importUser() throws Exception; 
	public List<StgImportUsers> getStgUsers();

	public List<Users> saveUsers(List<StgImportUsers> stgUsers);

}
