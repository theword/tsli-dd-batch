package com.tsli.dd.batch.service;
import java.util.List;

import javax.persistence.Column;

import com.tsli.dd.batch.entity.DdTAgentPerformance;
import com.tsli.dd.batch.entity.DdTAgentRankingPerformance;
import com.tsli.dd.batch.entity.DdTAgentStructure;
import com.tsli.dd.batch.entity.DdTKpiMapping;
import com.tsli.dd.batch.entity.DdTKpiTarget;
import com.tsli.dd.batch.model.AgentPerformance;
import com.tsli.dd.batch.model.SqlParameter;
public interface DatainService {

	List<DdTAgentPerformance> getStatingAgentPerformance(String calYM);
	List<DdTKpiMapping> getStatingKpiMapping(String calYM);
	List<DdTKpiTarget> getStatingKpiTarget(String string);
	List<DdTAgentRankingPerformance> getAgentRankingPerformance(String string);
	List<DdTAgentStructure> getAgentStructure(String closYm, String periodId);
	void importLevelMapping();
	void importZone();
}
