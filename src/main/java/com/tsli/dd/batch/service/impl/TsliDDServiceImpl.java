package com.tsli.dd.batch.service.impl;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tsli.dd.batch.entity.DdTAgentPerformance;
import com.tsli.dd.batch.entity.DdTAgentRankingPerformance;
import com.tsli.dd.batch.entity.DdTAgentStructure;
import com.tsli.dd.batch.entity.DdTKpiMapping;
import com.tsli.dd.batch.entity.DdTKpiTarget;
import com.tsli.dd.batch.model.AgentPerformance;
import com.tsli.dd.batch.repository.TsliDDAgentPeroformanceRepository;
import com.tsli.dd.batch.repository.TsliDDAgentRankingPerformanceRepository;
import com.tsli.dd.batch.repository.TsliDDAgentStructureRepository;
import com.tsli.dd.batch.repository.TsliDDKpiMappingRepository;
import com.tsli.dd.batch.repository.TsliDDKpiTargetRepository;
import com.tsli.dd.batch.service.TsliDDService;
import com.tsli.dd.batch.util.CalendarHelper;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

import jdk.internal.jline.internal.Log;
@Service
public class TsliDDServiceImpl implements TsliDDService {
	
	protected Logger log = LogManager.getLogger(getClass());
	@Autowired
    TsliDDAgentPeroformanceRepository tsliDDAgentPeroformanceRepository;
	
	@Autowired
    TsliDDKpiMappingRepository tsliDDKpiMappingRepository;
	
	@Autowired
    TsliDDKpiTargetRepository tsliDDKpiTargetRepository;
	
	@Autowired
    TsliDDAgentRankingPerformanceRepository tsliDDAgentRankingPerformanceRepository;
	
	@Autowired
    TsliDDAgentStructureRepository tsliDDAgentStructureRepository;
	
	@Autowired
	private EntityManager entityManager;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@SuppressWarnings("unchecked")
	@Transactional
	public String  saveAgentPerformnaceModel(List<DdTAgentPerformance>agentPerformance,String calYm) {
		String status="S01";
		deleteAgentPerformance(calYm);
		try {
			for (DdTAgentPerformance record : agentPerformance) {
				tsliDDAgentPeroformanceRepository.save(record);
			}
		}
//		try {
//			for (DdTAgentPerformance record : agentPerformance) {
//				DdTAgentPerformance model = new DdTAgentPerformance();
//				model.setClosYm(record.getClosYm());
//				model.setGroupChannel(record.getGroupChannel());
//				model.setChannel(record.getChannel());
//				model.setChannelName(record.getChannelName());
//				model.setZone(record.getZone());
//				model.setZoneCode(record.getZoneCode());
//				model.setZoneDesc(record.getZoneDesc());
//				model.setGmCode(record.getGmCode());
//				model.setAvpCode(record.getAvpCode());
//				model.setAlCode(record.getAlCode());
//				model.setAgentCode(record.getAgentCode());
//				model.setPositionName(record.getPositionName());
//				model.setAgentLevel(record.getAgentLevel());
//				model.setKpiValue1(record.getKpiValue1());
//				model.setKpiValue2(record.getKpiValue1());
//				model.setKpiValue3(record.getKpiValue1());
//				model.setKpiValue4(record.getKpiValue1());
//				model.setKpiValue5(record.getKpiValue1());
//				model.setKpiValue6(record.getKpiValue1());
//				model.setKpiValue7(record.getKpiValue1());
//				model.setKpiValue8(record.getKpiValue1());
//				model.setKpiValue9(record.getKpiValue1());
//				model.setKpiValue10(record.getKpiValue1());
//				model.setKpiValue11(record.getKpiValue1());
//				model.setKpiValue12(record.getKpiValue1());
//				model.setKpiValue13(record.getKpiValue1());
//				model.setKpiValue14(record.getKpiValue1());
//				model.setKpiValue15(record.getKpiValue1());
//				model.setKpiValue16(record.getKpiValue1());
//				model.setKpiValue17(record.getKpiValue1());
//				model.setKpiValue18(record.getKpiValue1());
//				model.setKpiValue19(record.getKpiValue1());
//				model.setKpiValue20(record.getKpiValue1());
//				model.setKpiValue21(record.getKpiValue1());
//				model.setKpiValue22(record.getKpiValue1());
//				model.setKpiValue23(record.getKpiValue1());
//				model.setKpiValue24(record.getKpiValue1());
//				model.setKpiValue25(record.getKpiValue1());
//				model.setKpiValue26(record.getKpiValue1());
//				model.setKpiValue27(record.getKpiValue1());
//				model.setKpiValue28(record.getKpiValue1());
//				model.setKpiValue29(record.getKpiValue1());
//				model.setKpiValue30(record.getKpiValue1());
//				model.setKpiValue31(record.getKpiValue1());
//				model.setKpiValue32(record.getKpiValue1());
//				model.setKpiValue33(record.getKpiValue1());
//				model.setKpiValue34(record.getKpiValue1());
//				model.setKpiValue35(record.getKpiValue1());
//				model.setKpiValue36(record.getKpiValue1());
//				model.setKpiValue37(record.getKpiValue1());
//				model.setKpiValue38(record.getKpiValue1());
//				model.setKpiValue39(record.getKpiValue1());
//				model.setKpiValue40(record.getKpiValue1());
//				model.setRegDtm(CalendarHelper.getCurrentDateTime().getTime());
//				model.setPeNoReg(record.getPeNoReg());
//				model.setChgDtm(CalendarHelper.getCurrentDateTime().getTime());
//				model.setPeNoChg(record.getPeNoChg());
//				model.setSnapFlag(record.getSnapFlag());
//				model.setSnapDate(CalendarHelper.getCurrentDateTime().getTime());
//				tsliDDAgentPeroformanceRepository.save(model);
//			//	Log.info("Model: "+model.getId().toString());
//			}
//		}
		catch(Exception e) {
			status="E01";
			}
		
		return status;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	private boolean deleteAgentPerformance(String calYM){
		String sql = "delete from ddmaster.DD_T_AGENT_PERFORMANCE where CLOS_YM=?" ;
		jdbcTemplate.update(sql,calYM);
		log.info("sql: "+sql);
		return true;

	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	private boolean deleteKpiMapping(String calYM){
		String sql = "delete from ddmaster.DD_T_KPI_MAPPING" ;
//		DdTKpiMapping foo = new DdTKpiMapping();
//		entityManager.persist(foo);
//		entityManager.createQuery("delete from ddmaster.DD_T_KPI_MAPPING where ID = :id")
//		.setParameter("id", foo.getId())
//		.executeUpdate();
		jdbcTemplate.update(sql);
			log.info("sql: "+sql);
//			List<DdTAgentRankingPerformance> listModel = mappingAgentRankingPerformance(list);
			return true;

	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public String saveKpiMapping(List<DdTKpiMapping>kpiMappingModel) {
		
		deleteKpiMapping(null);
		
		String status="S01";
		try {
			for (DdTKpiMapping record : kpiMappingModel) {
				tsliDDKpiMappingRepository.save(record);
			}
		}
		catch(Exception e) {
			status="E01";
			}
		
		return status;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	private boolean deleteKpiTarget(String calYM){
		String sql = "delete from ddmaster.DD_T_KPI_TARGET" ;
		jdbcTemplate.update(sql);
			log.info("sql: "+sql);
			return true;

	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public String saveKpiTarget(List<DdTKpiTarget>kpiMappingTarget) {
		String status="S01";
		deleteKpiTarget(null);
		try {
			for (DdTKpiTarget record : kpiMappingTarget) {
				tsliDDKpiTargetRepository.save(record);
			}
		}
		catch(Exception e) {
			status="E01";
			}
		
		return status;
	}
	
	@Transactional
	private boolean deleteAgentRankingPerformance(String calYM){
		String sql = "delete from ddmaster.DD_T_AGENT_RANKING_PERFORMANCE" ;
		jdbcTemplate.update(sql);
			log.info("sql: "+sql);
			return true;

	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public String saveAgentRankingPerformance(List<DdTAgentRankingPerformance>agentRankingPerformance) {
		String status="S01";
		
		deleteAgentRankingPerformance(null);
		try {
			for (DdTAgentRankingPerformance record : agentRankingPerformance) {
				tsliDDAgentRankingPerformanceRepository.save(record);
			}
		}
		catch(Exception e) {
			status="E01";
			}
		
		return status;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	private boolean deleteAgentStructure(String calYM){
		String sql = "delete from ddmaster.DD_T_AGENT_STRUCTURE" ;
		jdbcTemplate.update(sql);
			log.info("sql: "+sql);
			return true;

	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public String saveAgentStructure(List<DdTAgentStructure>agentStructure) {
		String status="S01";
		deleteAgentStructure(null);
		try {
			for (DdTAgentStructure record : agentStructure) {
				tsliDDAgentStructureRepository.save(record);
			}
		}
		catch(Exception e) {
			status="E01";
			}
		
		return status;
	}
}
