package com.tsli.dd.batch.service.impl;
import java.text.SimpleDateFormat;
import java.util.List;
//import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tsli.dd.batch.entity.DdTAgentPerformance;
import com.tsli.dd.batch.entity.DdTAgentRankingPerformance;
import com.tsli.dd.batch.entity.DdTAgentStructure;
import com.tsli.dd.batch.entity.DdTKpiMapping;
import com.tsli.dd.batch.entity.DdTKpiTarget;
import com.tsli.dd.batch.service.DatainService;
import com.tsli.dd.batch.util.ObjectUtils;

@Service
@SuppressWarnings({"unchecked","deprecation"})
public class DatainServiceImpl implements DatainService {
	protected Logger log = LogManager.getLogger(getClass());

	@Autowired
	private EntityManager entityManager;
	SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");  

	@Transactional
	public List<DdTAgentPerformance> getStatingAgentPerformance(String calYM) {

		String sql = 
				" SELECT " + 
						" nvl(mas.CLOS_YM,pd.CLOS_YM) as CLOS_YM," + 
						" nvl(mas.GROUP_CHANNEL,pd.GROUP_CHANNEL)GROUP_CHANNEL," + 
						" nvl(mas.CHANNEL,pd.CHANNEL)CHANNEL," + 
						" nvl(mas.CHANNEL_NAME,pd.CHANNEL_NAME)CHANNEL_NAME," + 
						" nvl(mas.ZONE,pd.ZONE)ZONE," + 
						" nvl(mas.ZONE_CODE,pd.ZONE_CODE)ZONE_CODE," + 
						" nvl(mas.ZONE_DESC,pd.ZONE_DESC)ZONE_DESC," + 
						" nvl(mas.GM_CODE,pd.GM_CODE)GM_CODE," + 
						" nvl(mas.AVP_CODE,pd.AVP_CODE)AVP_CODE," + 
						" nvl(mas.AL_CODE,pd.AL_CODE)AL_CODE," + 
						" nvl(mas.AGENT_CODE,pd.AGENT_CODE)AGENT_CODE," + 
						" nvl(mas.POSITION_NAME,pd.POSITION_NAME)POSITION_NAME," + 
						" nvl(mas.AGENT_LEVEL,pd.AGENT_LEVEL)AGENT_LEVEL," + 
						" nvl(mas.KPI_VALUE_1+nvl(pd.KPI_VALUE_1,0),0)KPI_VALUE_1," + 
						" nvl(mas.KPI_VALUE_2+nvl(pd.KPI_VALUE_2,0),0)KPI_VALUE_2," + 
						" nvl(mas.KPI_VALUE_3+nvl(pd.KPI_VALUE_3,0),0)KPI_VALUE_3," + 
						" nvl(mas.KPI_VALUE_4+nvl(pd.KPI_VALUE_4,0),0)KPI_VALUE_4," + 
						" nvl(mas.KPI_VALUE_5+nvl(pd.KPI_VALUE_5,0),0)KPI_VALUE_5," + 
						" nvl(mas.KPI_VALUE_6+nvl(pd.KPI_VALUE_6,0),0)KPI_VALUE_6," + 
						" nvl(mas.KPI_VALUE_7+nvl(pd.KPI_VALUE_7,0),0)KPI_VALUE_7," + 
						" nvl(mas.KPI_VALUE_8+nvl(pd.KPI_VALUE_8,0),0)KPI_VALUE_8," + 
						" nvl(mas.KPI_VALUE_9+nvl(pd.KPI_VALUE_9,0),0)KPI_VALUE_9," + 
						" nvl(mas.KPI_VALUE_10+nvl(pd.KPI_VALUE_10,0),0)KPI_VALUE_10," + 
						" nvl(mas.KPI_VALUE_11+nvl(pd.KPI_VALUE_11,0),0)KPI_VALUE_11," + 
						" nvl(mas.KPI_VALUE_12+nvl(pd.KPI_VALUE_12,0),0)KPI_VALUE_12," + 
						" nvl(mas.KPI_VALUE_13+nvl(pd.KPI_VALUE_13,0),0)KPI_VALUE_13," + 
						" nvl(mas.KPI_VALUE_14+nvl(pd.KPI_VALUE_14,0),0)KPI_VALUE_14," + 
						" nvl(mas.KPI_VALUE_15+nvl(pd.KPI_VALUE_15,0),0)KPI_VALUE_15," + 
						" nvl(mas.KPI_VALUE_16+nvl(pd.KPI_VALUE_16,0),0)KPI_VALUE_16," + 
						" nvl(mas.KPI_VALUE_17+nvl(pd.KPI_VALUE_17,0),0)KPI_VALUE_17," + 
						" nvl(mas.KPI_VALUE_18+nvl(pd.KPI_VALUE_18,0),0)KPI_VALUE_18," + 
						" nvl(mas.KPI_VALUE_19+nvl(pd.KPI_VALUE_19,0),0)KPI_VALUE_19," + 
						" nvl(mas.KPI_VALUE_20+nvl(pd.KPI_VALUE_20,0),0)KPI_VALUE_20," + 
						" nvl(mas.KPI_VALUE_21+nvl(pd.KPI_VALUE_21,0),0)KPI_VALUE_21," + 
						" nvl(mas.KPI_VALUE_22+nvl(pd.KPI_VALUE_22,0),0)KPI_VALUE_22," + 
						" nvl(mas.KPI_VALUE_23+nvl(pd.KPI_VALUE_23,0),0)KPI_VALUE_23," + 
						" nvl(mas.KPI_VALUE_24+nvl(pd.KPI_VALUE_24,0),0)KPI_VALUE_24," + 
						" nvl(mas.KPI_VALUE_25+nvl(pd.KPI_VALUE_25,0),0)KPI_VALUE_25," + 
						" nvl(mas.KPI_VALUE_26+nvl(pd.KPI_VALUE_26,0),0)KPI_VALUE_26," + 
						" nvl(mas.KPI_VALUE_27+nvl(pd.KPI_VALUE_27,0),0)KPI_VALUE_27," + 
						" nvl(mas.KPI_VALUE_28+nvl(pd.KPI_VALUE_28,0),0)KPI_VALUE_28," + 
						" nvl(mas.KPI_VALUE_29+nvl(pd.KPI_VALUE_29,0),0)KPI_VALUE_29," + 
						" nvl(mas.KPI_VALUE_30+nvl(pd.KPI_VALUE_30,0),0)KPI_VALUE_30," + 
						" nvl(mas.KPI_VALUE_31+nvl(pd.KPI_VALUE_31,0),0)KPI_VALUE_31," + 
						" nvl(mas.KPI_VALUE_32+nvl(pd.KPI_VALUE_32,0),0)KPI_VALUE_32," + 
						" nvl(mas.KPI_VALUE_33+nvl(pd.KPI_VALUE_33,0),0)KPI_VALUE_33," + 
						" nvl(mas.KPI_VALUE_34+nvl(pd.KPI_VALUE_34,0),0)KPI_VALUE_34," + 
						" nvl(mas.KPI_VALUE_35+nvl(pd.KPI_VALUE_35,0),0)KPI_VALUE_35," + 
						" nvl(mas.KPI_VALUE_36+nvl(pd.KPI_VALUE_36,0),0)KPI_VALUE_36," + 
						" nvl(mas.KPI_VALUE_37+nvl(pd.KPI_VALUE_37,0),0)KPI_VALUE_37," + 
						" nvl(mas.KPI_VALUE_38+nvl(pd.KPI_VALUE_38,0),0)KPI_VALUE_38," + 
						" nvl(mas.KPI_VALUE_39+nvl(pd.KPI_VALUE_39,0),0)KPI_VALUE_39," + 
						" nvl(mas.KPI_VALUE_40+nvl(pd.KPI_VALUE_40,0),0)KPI_VALUE_40," + 
						" nvl(pd.REG_DTM,mas.REG_DTM)REG_DTM," + 
						" nvl(pd.PE_NO_REG,mas.PE_NO_REG)PE_NO_REG," + 
						" nvl(pd.CHG_DTM,mas.CHG_DTM)CHG_DTM," + 
						" nvl(pd.PE_NO_CHG,mas.PE_NO_CHG)PE_NO_CHG," + 
						" nvl(pd.SNAP_FLAG,mas.SNAP_FLAG)SNAP_FLAG," + 
						" nvl(pd.SNAP_DATE,mas.SNAP_DATE)SNAP_DATE" + 
						" FROM" + 
						" DATAHUB_PROD.DD_T_AGENT_PERFORMANCE mas " + 
						"     left JOIN DATAHUB_PROD.DD_T_AGENT_PERFORMANCE_DAILY pd ON pd.CLOS_YM=mas.CLOS_YM AND nvl(mas.agent_code, '-') = nvl(pd.AGENT_CODE, '-') AND mas.AGENT_LEVEL=pd.AGENT_LEVEL " + 
						"   and nvl(mas.zone, '-') = nvl(pd.zone, '-') and nvl(mas.group_channel, '-') = nvl(pd.group_channel, '-') "
						+ "WHERE mas.CLOS_YM=:calYM " + 
						" union" + 
						" SELECT pd.CLOS_YM as CLOS_YM,GROUP_CHANNEL,CHANNEL,CHANNEL_NAME,ZONE,ZONE_CODE,ZONE_DESC,GM_CODE," + 
						" AVP_CODE,AL_CODE,AGENT_CODE,POSITION_NAME,AGENT_LEVEL,KPI_VALUE_1," + 
						" KPI_VALUE_2,KPI_VALUE_3,KPI_VALUE_4,KPI_VALUE_5,KPI_VALUE_6,KPI_VALUE_7," + 
						" KPI_VALUE_8,KPI_VALUE_9,KPI_VALUE_10,KPI_VALUE_11,KPI_VALUE_12,KPI_VALUE_13," + 
						" KPI_VALUE_14,KPI_VALUE_15,KPI_VALUE_16,KPI_VALUE_17,KPI_VALUE_18,KPI_VALUE_19," + 
						" KPI_VALUE_20,KPI_VALUE_21,KPI_VALUE_22,KPI_VALUE_23,KPI_VALUE_24,KPI_VALUE_25," + 
						" KPI_VALUE_26,KPI_VALUE_27,KPI_VALUE_28,KPI_VALUE_29,KPI_VALUE_30,KPI_VALUE_31," + 
						" KPI_VALUE_32,KPI_VALUE_33,KPI_VALUE_34,KPI_VALUE_35,KPI_VALUE_36,KPI_VALUE_37," + 
						" KPI_VALUE_38,KPI_VALUE_39,KPI_VALUE_40,REG_DTM,PE_NO_REG,CHG_DTM,PE_NO_CHG,SNAP_FLAG,SNAP_DATE" + 
						" " + 
						"  FROM DATAHUB_PROD.DD_T_AGENT_PERFORMANCE_DAILY pd" + 
						" where not EXISTS  (select * from DATAHUB_PROD.DD_T_AGENT_PERFORMANCE mas " + 
						" where pd.CLOS_YM=mas.CLOS_YM AND nvl(mas.agent_code,'-') = nvl(pd.AGENT_CODE,'-') "
						+ " and nvl(mas.zone, '-') = nvl(pd.zone, '-') and nvl(mas.group_channel, '-') = nvl(pd.group_channel, '-') "
						+ " AND mas.AGENT_LEVEL=pd.AGENT_LEVEL" + 
						" and mas.CLOS_YM=:calYM" + 
						" ) and pd.CLOS_YM=:calYM";

		Query query = this.entityManager.createNativeQuery(sql).unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(
						Transformers.ALIAS_TO_ENTITY_MAP
						);
		if(StringUtils.contains(sql, ":calYM")){
			query.setParameter("calYM", calYM);
		}

		List<DdTAgentPerformance> listModel = 	ObjectUtils.toObject(query.getResultList(), DdTAgentPerformance.class );

		log.info("sql: "+sql);
		log.info("result size: "+listModel.size());

		log.info("sql: "+sql);
		log.info("result size: "+listModel.size());
		return listModel;
	}


	@Transactional
	public List<DdTKpiMapping> getStatingKpiMapping(String calYM) {
		String sql = 
				"select * from DATAHUB_PROD.DD_T_KPI_MAPPING" ;
		Query query = this.entityManager.createNativeQuery(sql).unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(
						Transformers.ALIAS_TO_ENTITY_MAP
						);

		List<DdTKpiMapping> listModel = 	ObjectUtils.toObject(query.getResultList(), DdTKpiMapping.class );

		log.info("sql: "+sql);
		log.info("result size: "+listModel.size());
		return listModel;
	}

	@Transactional
	public List<DdTKpiTarget> getStatingKpiTarget(String calYM) {
		String sql = 
				"select * from DATAHUB_PROD.DD_T_KPI_TARGET" ;

		Query query = this.entityManager.createNativeQuery(sql).unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(
						Transformers.ALIAS_TO_ENTITY_MAP
						);

		List<DdTKpiTarget> listModel = 	ObjectUtils.toObject(query.getResultList(), DdTKpiTarget.class );

		log.info("sql: "+sql);
		log.info("result size: "+listModel.size());

		return listModel;
	}

	@Transactional
	public List<DdTAgentRankingPerformance> getAgentRankingPerformance(String calYM){
		String sql = 
				"select * from DATAHUB_PROD.DD_T_AGENT_RANKING_PERFORMANCE where clos_ym = :calYM" ;
		Query query = this.entityManager.createNativeQuery(sql).unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(
						Transformers.ALIAS_TO_ENTITY_MAP
						);

		if(StringUtils.contains(sql, ":calYM")){
			query.setParameter("calYM", calYM);
		}	
		List<Map<String, Object>> data = query.getResultList();
		List<DdTAgentRankingPerformance> listModel = 	ObjectUtils.toObject(data, DdTAgentRankingPerformance.class );

		log.info("sql: "+sql);
		log.info("result size: "+listModel.size());
		return listModel;

	}

	@Transactional
	public List<DdTAgentStructure> getAgentStructure(String closYm, String periodId) {
		String sql = "select * from DATAHUB_PROD.DD_T_AGENT_STRUCTURE" ;
		Query query = this.entityManager.createNativeQuery(sql).unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(
						Transformers.ALIAS_TO_ENTITY_MAP
						);

		List<DdTAgentStructure> listModel = 	ObjectUtils.toObject(query.getResultList(), DdTAgentStructure.class );
		return listModel;
	}

	@Transactional
	public void importLevelMapping() {
		int row;
		String sql = "delete DD_T_LEVEL_MAPPING" ;
		row = this.entityManager.createNativeQuery( sql ).executeUpdate();
		log.info("delete DD_T_LEVEL_MAPPING: " + row + " row ");
		sql = "insert into DD_T_LEVEL_MAPPING (ID, LEVEL_NAME, GA, SFC) select null, LEVELS, GA, SFC from  DATAHUB_PROD.DD_T_LEVEL_MAPPING" ;
		row = this.entityManager.createNativeQuery(sql).executeUpdate();
		log.info("insert DD_T_LEVEL_MAPPING: " + row + " row ");
	}
	
	@Transactional
	public void importZone() {
		int row;
		String sql = "delete DD_T_ZONE" ;
		row = this.entityManager.createNativeQuery( sql ).executeUpdate();
		log.info("delete DD_T_ZONE: " + row + " row ");
		sql = "INSERT INTO DD_T_ZONE (ID, CHANNEL, ZONE, ZONE_NAME ) SELECT NULL, CHANNEL, ZONE,  ZONE_NAME FROM  DATAHUB_PROD.DD_T_ZONE" ;
		row = this.entityManager.createNativeQuery(sql).executeUpdate();
		log.info("insert DD_T_ZONE: " + row + " row ");
	}
	
//	
//	" insert into DD_T_LEVEL_MAPPING (ID, LEVEL_NAME, GA, SFC) select null, LEVELS, GA, SFC from  DATAHUB_PROD.DD_T_LEVEL_MAPPING ";


}
