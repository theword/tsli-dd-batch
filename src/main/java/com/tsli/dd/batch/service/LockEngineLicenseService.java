package com.tsli.dd.batch.service;

import com.deverhood.lock.manager.resource.ResAuth;
import com.deverhood.lock.manager.resource.ResAuthToken;
import com.deverhood.util.ApiClient;


public class LockEngineLicenseService {

	public static final String encode( String code ) {
		ResAuth access = new ResAuth();
		access.setUser(code);
		return ResAuthToken.generateToken(access).getToken();
	}
	
	public static final String decode( String token ) {
		String code = ApiClient.decodeToken( token, ResAuthToken.PROTOCOL);
		return ResAuth.fromJsonString(code).getUser();
	}
	
	public static final Integer decodeLicense( String token ) {
		String code = ApiClient.decodeToken( token, ResAuthToken.PROTOCOL);
		return Integer.parseInt(ResAuth.fromJsonString(code).getPlatformInfo());
		
	}
}
