package com.tsli.dd.batch.service;

import java.util.Date;
import java.util.List;

import com.tsli.dd.batch.entity.DdTClosCalendar;
import com.tsli.dd.batch.entity.StgScheduleLog;

public interface ClosCalendarService {

	public DdTClosCalendar getDdTClosCalendarByimportDate(String importDate);

	public DdTClosCalendar getddTClosCalendar(String importDate);

	public DdTClosCalendar getNearestClosYm();

	public StgScheduleLog getBatchEndDay();

	public StgScheduleLog getBatchDaily();

	public StgScheduleLog updateBatchStatus(Date startDate, Date endDate, String msg, Integer status, String batchType);

	public void importClosCalendar();

	public List<DdTClosCalendar> getListDdTClosCalendarByimportDate();
	
	public List<DdTClosCalendar> getListDdTClosCalendarByimportDate(String importDate);
	
	public List<DdTClosCalendar> getListDdTClosCalendarByClosYM(String closYM);
}
