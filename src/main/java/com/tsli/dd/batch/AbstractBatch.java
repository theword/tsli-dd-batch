package com.tsli.dd.batch;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.springframework.boot.CommandLineRunner;
import org.apache.logging.log4j.Logger;

public abstract class AbstractBatch implements CommandLineRunner {
	protected Logger log = LogManager.getLogger(getClass());
	protected String batchCode = "";
	protected String batchId = "";
	final String BLANK_ARG = "-";
	protected Map<String, String> paramMap = new HashMap<String, String>();
	protected String argCalYM = null;
	protected String argParams = "";
	protected Long argPeriodId = null;
	protected String argImportDate = null;
	protected String argBatchType = null;	// Daily or end day
	protected Date startDate;
	protected Date endDate ;
	//@Override
	public void run(String... args) throws Exception {
		startDate = new Date();
		log.info("START " + batchCode + " : " + startDate.toString());
		this.initail(args);
		this.setArgruments(args);
		this.start(args);
		endDate = new Date();
		log.info("END " + batchCode + " : " + endDate.toString());

		log.info("Summary\t: " + batchCode);
		log.info("Start\t: " + startDate.toString());
		log.info("End\t: " + endDate.toString());
		log.info("Total\t: " + getTimeDiff(startDate, endDate));

	}

	public abstract void initail(String... args) throws Exception;
	public abstract void start(String... args) throws Exception;
	
	protected void setArgruments(String... args) {
		if (args != null) {
			if (args.length > 0) {
				if (validateClosYM(args[0]) && !BLANK_ARG.equals(args[0])) {
					this.argCalYM = args[0];
					// log.info("Argrument[1]
					// argPeriodYear"+argPeriodYear+"\targPeriodMonth:"+argPeriodMonth);
				}
			}
			if (args.length > 1 && !BLANK_ARG.equals(args[1])) {
				if (StringUtils.isNotBlank((args[1])) && StringUtils.isNumeric((args[1]))) {
//					Integer day = new Integer(args[2].substring(6, 8));
//					Integer month = new Integer(args[2].substring(4, 6));
//					Integer year = new Integer(args[2].substring(0, 4));
//					this.argPaymentDate = Calendar.getInstance();
//					this.argPaymentDate = new GregorianCalendar(year, month, day);
					this.argImportDate =args[1];
					// this.argPeriodId = Long.parseLong(args[2]);
					// log.info("Argrument[2] argPeriodId"+argPeriodId);
				}
			}
			if (args.length > 2 &&!BLANK_ARG.equals(args[2])) {
				if (StringUtils.isNotBlank((args[2]))) {
					this.batchId = args[2];
				}
			}
			
			if (args.length > 3 &&!BLANK_ARG.equals(args[3])) {
				if (StringUtils.isNotBlank((args[3]))) {
					this.argBatchType = args[3];
				}
			}
		}
	}
	
	private boolean validateClosYM(String closYM) {
		if (StringUtils.isNotBlank(closYM) && StringUtils.isNumeric(closYM)  && StringUtils.length(closYM) == 6) {
			return true;
		}
		return false;
	}
	
	private String getTimeDiff(Date startDate,Date endDate){
		String timeDiff="";					
		long diff = endDate.getTime() - startDate.getTime();

		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);
		
		timeDiff=String.format("%02d", diffHours)+":"+String.format("%02d", diffMinutes)+":"+String.format("%02d", diffSeconds);
		if(diffDays>0){
			timeDiff = "("+diffDays+" day)"+" "+ timeDiff;
		}
		return timeDiff;
	}
}
