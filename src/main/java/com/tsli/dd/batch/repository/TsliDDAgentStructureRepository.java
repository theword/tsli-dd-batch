package com.tsli.dd.batch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tsli.dd.batch.entity.DdTAgentRankingPerformance;
import com.tsli.dd.batch.entity.DdTAgentStructure;
import com.tsli.dd.batch.entity.DdTKpiMapping;

public interface TsliDDAgentStructureRepository extends JpaRepository<DdTAgentStructure, Long>{

}
