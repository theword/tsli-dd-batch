package com.tsli.dd.batch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tsli.dd.batch.entity.DdTKpiTarget;

public interface TsliDDKpiTargetRepository extends JpaRepository<DdTKpiTarget, Long> {

}
