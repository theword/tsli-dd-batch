package com.tsli.dd.batch.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.tsli.dd.batch.entity.Users;

public interface TsliDDUserRepository extends JpaRepository<Users, Long> {

	@Query("select u from Users u where u.username = ?1")
	Users findByUserName(String userName);
	
	@Query("select count(u) from Users u where u.status = 'A'")
	Integer countActiveLicense();
	
	@Modifying
	@Query("delete Users")
	void deleteAll();
}
