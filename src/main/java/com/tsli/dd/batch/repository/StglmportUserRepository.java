package com.tsli.dd.batch.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.tsli.dd.batch.entity.StgImportUsers;

public interface StglmportUserRepository extends JpaRepository<StgImportUsers, Long> {
	@Transactional
	@Modifying
	@Query("update StgImportUsers c set c.statusToLockengine = :statusToLockengine , c.b3Updatedate =:sysdate , c.remark = :remark WHERE c.userName = :userName")
	void setStatusToLockengin(@Param("userName") String userName, @Param("statusToLockengine") String statusToLockengine, @Param("sysdate") Date sysdate,@Param("remark")  String remark);

}
