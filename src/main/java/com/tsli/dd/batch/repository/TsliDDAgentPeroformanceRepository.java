package com.tsli.dd.batch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tsli.dd.batch.entity.DdTAgentPerformance;

public interface TsliDDAgentPeroformanceRepository extends JpaRepository<DdTAgentPerformance, Long>{

}
