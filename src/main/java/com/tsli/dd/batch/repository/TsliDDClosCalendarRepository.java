package com.tsli.dd.batch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.tsli.dd.batch.entity.DdTClosCalendar;
import com.tsli.dd.batch.entity.DdTKpiTarget;

public interface TsliDDClosCalendarRepository extends JpaRepository<DdTClosCalendar, Long> {
	
	@Query("select d from DdTClosCalendar d where d.flagEndClosingYmd = 'F' order by d.closYm")
	List<DdTClosCalendar> getNearestClosYm();
}
