package com.tsli.dd.batch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tsli.dd.batch.entity.DdTAgentRankingPerformance;

public interface TsliDDAgentRankingPerformanceRepository extends JpaRepository<DdTAgentRankingPerformance, Long>{

}
