package com.tsli.dd.batch.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tsli.dd.batch.entity.StgScheduleLog;


public interface StgScheduleLogRepository extends JpaRepository<StgScheduleLog, Integer> {

	@Query("select distinct s from StgScheduleLog s where s.batchType = :batchType")
	public StgScheduleLog findByBatchType( @Param("batchType") Character batchType);
}
