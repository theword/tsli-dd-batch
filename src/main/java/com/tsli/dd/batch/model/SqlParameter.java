package com.tsli.dd.batch.model;

import javax.persistence.ParameterMode;

public class SqlParameter {
	private int index;
	private String name;
	private Class classType;
	private ParameterMode parameterMode;
	private Object value;

	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Class getClassType() {
		return classType;
	}
	public void setClassType(Class classType) {
		this.classType = classType;
	}
	public ParameterMode getParameterMode() {
		return parameterMode;
	}
	public void setParameterMode(ParameterMode parameterMode) {
		this.parameterMode = parameterMode;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
}
