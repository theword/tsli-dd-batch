package com.tsli.dd.batch.model;

import java.math.BigDecimal;
import java.util.Date;

public class AgentPerformance {
    private Integer id;
    private String closYm;
    private String groupChannel;
    private String channel;
    private String channelName;
    private String zone;
    private String zoneCode;
    private String zoneDesc;
    private String gmCode;
    private String avpCode;
    private String alCode;
    private String agentCode;
    private String positionName;
    private String agentLevel;
    private BigDecimal kpiValue1;
    private BigDecimal kpiValue2;
    private BigDecimal kpiValue3;
    private BigDecimal kpiValue4;
    private BigDecimal kpiValue5;
    private BigDecimal kpiValue6;
    private BigDecimal kpiValue7;
    private BigDecimal kpiValue8;
    private BigDecimal kpiValue9;
    private BigDecimal kpiValue10;
    private BigDecimal kpiValue11;
    private BigDecimal kpiValue12;
    private BigDecimal kpiValue13;
    private BigDecimal kpiValue14;
    private BigDecimal kpiValue15;
    private BigDecimal kpiValue16;
    private BigDecimal kpiValue17;
    private BigDecimal kpiValue18;
    private BigDecimal kpiValue19;
    private BigDecimal kpiValue20;
    private BigDecimal kpiValue21;
    private BigDecimal kpiValue22;
    private BigDecimal kpiValue23;
    private BigDecimal kpiValue24;
    private BigDecimal kpiValue25;
    private BigDecimal kpiValue26;
    private BigDecimal kpiValue27;
    private BigDecimal kpiValue28;
    private BigDecimal kpiValue29;
    private BigDecimal kpiValue30;
    private BigDecimal kpiValue31;
    private BigDecimal kpiValue32;
    private BigDecimal kpiValue33;
    private BigDecimal kpiValue34;
    private BigDecimal kpiValue35;
    private BigDecimal kpiValue36;
    private BigDecimal kpiValue37;
    private BigDecimal kpiValue38;
    private BigDecimal kpiValue39;
    private BigDecimal kpiValue40;
    private Date regDtm;
    private String peNoReg;
    private Date chgDtm;
    private String peNoChg;
    private String snapFlag;
    private Date snapDate;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getClosYm() {
		return closYm;
	}
	public void setClosYm(String closYm) {
		this.closYm = closYm;
	}
	public String getGroupChannel() {
		return groupChannel;
	}
	public void setGroupChannel(String groupChannel) {
		this.groupChannel = groupChannel;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getZoneCode() {
		return zoneCode;
	}
	public void setZoneCode(String zoneCode) {
		this.zoneCode = zoneCode;
	}
	public String getZoneDesc() {
		return zoneDesc;
	}
	public void setZoneDesc(String zoneDesc) {
		this.zoneDesc = zoneDesc;
	}
	public String getGmCode() {
		return gmCode;
	}
	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}
	public String getAvpCode() {
		return avpCode;
	}
	public void setAvpCode(String avpCode) {
		this.avpCode = avpCode;
	}
	public String getAlCode() {
		return alCode;
	}
	public void setAlCode(String alCode) {
		this.alCode = alCode;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	public String getAgentLevel() {
		return agentLevel;
	}
	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}
	public BigDecimal getKpiValue1() {
		return kpiValue1;
	}
	public void setKpiValue1(BigDecimal kpiValue1) {
		this.kpiValue1 = kpiValue1;
	}
	public BigDecimal getKpiValue2() {
		return kpiValue2;
	}
	public void setKpiValue2(BigDecimal kpiValue2) {
		this.kpiValue2 = kpiValue2;
	}
	public BigDecimal getKpiValue3() {
		return kpiValue3;
	}
	public void setKpiValue3(BigDecimal kpiValue3) {
		this.kpiValue3 = kpiValue3;
	}
	public BigDecimal getKpiValue4() {
		return kpiValue4;
	}
	public void setKpiValue4(BigDecimal kpiValue4) {
		this.kpiValue4 = kpiValue4;
	}
	public BigDecimal getKpiValue5() {
		return kpiValue5;
	}
	public void setKpiValue5(BigDecimal kpiValue5) {
		this.kpiValue5 = kpiValue5;
	}
	public BigDecimal getKpiValue6() {
		return kpiValue6;
	}
	public void setKpiValue6(BigDecimal kpiValue6) {
		this.kpiValue6 = kpiValue6;
	}
	public BigDecimal getKpiValue7() {
		return kpiValue7;
	}
	public void setKpiValue7(BigDecimal kpiValue7) {
		this.kpiValue7 = kpiValue7;
	}
	public BigDecimal getKpiValue8() {
		return kpiValue8;
	}
	public void setKpiValue8(BigDecimal kpiValue8) {
		this.kpiValue8 = kpiValue8;
	}
	public BigDecimal getKpiValue9() {
		return kpiValue9;
	}
	public void setKpiValue9(BigDecimal kpiValue9) {
		this.kpiValue9 = kpiValue9;
	}
	public BigDecimal getKpiValue10() {
		return kpiValue10;
	}
	public void setKpiValue10(BigDecimal kpiValue10) {
		this.kpiValue10 = kpiValue10;
	}
	public BigDecimal getKpiValue11() {
		return kpiValue11;
	}
	public void setKpiValue11(BigDecimal kpiValue11) {
		this.kpiValue11 = kpiValue11;
	}
	public BigDecimal getKpiValue12() {
		return kpiValue12;
	}
	public void setKpiValue12(BigDecimal kpiValue12) {
		this.kpiValue12 = kpiValue12;
	}
	public BigDecimal getKpiValue13() {
		return kpiValue13;
	}
	public void setKpiValue13(BigDecimal kpiValue13) {
		this.kpiValue13 = kpiValue13;
	}
	public BigDecimal getKpiValue14() {
		return kpiValue14;
	}
	public void setKpiValue14(BigDecimal kpiValue14) {
		this.kpiValue14 = kpiValue14;
	}
	public BigDecimal getKpiValue15() {
		return kpiValue15;
	}
	public void setKpiValue15(BigDecimal kpiValue15) {
		this.kpiValue15 = kpiValue15;
	}
	public BigDecimal getKpiValue16() {
		return kpiValue16;
	}
	public void setKpiValue16(BigDecimal kpiValue16) {
		this.kpiValue16 = kpiValue16;
	}
	public BigDecimal getKpiValue17() {
		return kpiValue17;
	}
	public void setKpiValue17(BigDecimal kpiValue17) {
		this.kpiValue17 = kpiValue17;
	}
	public BigDecimal getKpiValue18() {
		return kpiValue18;
	}
	public void setKpiValue18(BigDecimal kpiValue18) {
		this.kpiValue18 = kpiValue18;
	}
	public BigDecimal getKpiValue19() {
		return kpiValue19;
	}
	public void setKpiValue19(BigDecimal kpiValue19) {
		this.kpiValue19 = kpiValue19;
	}
	public BigDecimal getKpiValue20() {
		return kpiValue20;
	}
	public void setKpiValue20(BigDecimal kpiValue20) {
		this.kpiValue20 = kpiValue20;
	}
	public BigDecimal getKpiValue21() {
		return kpiValue21;
	}
	public void setKpiValue21(BigDecimal kpiValue21) {
		this.kpiValue21 = kpiValue21;
	}
	public BigDecimal getKpiValue22() {
		return kpiValue22;
	}
	public void setKpiValue22(BigDecimal kpiValue22) {
		this.kpiValue22 = kpiValue22;
	}
	public BigDecimal getKpiValue23() {
		return kpiValue23;
	}
	public void setKpiValue23(BigDecimal kpiValue23) {
		this.kpiValue23 = kpiValue23;
	}
	public BigDecimal getKpiValue24() {
		return kpiValue24;
	}
	public void setKpiValue24(BigDecimal kpiValue24) {
		this.kpiValue24 = kpiValue24;
	}
	public BigDecimal getKpiValue25() {
		return kpiValue25;
	}
	public void setKpiValue25(BigDecimal kpiValue25) {
		this.kpiValue25 = kpiValue25;
	}
	public BigDecimal getKpiValue26() {
		return kpiValue26;
	}
	public void setKpiValue26(BigDecimal kpiValue26) {
		this.kpiValue26 = kpiValue26;
	}
	public BigDecimal getKpiValue27() {
		return kpiValue27;
	}
	public void setKpiValue27(BigDecimal kpiValue27) {
		this.kpiValue27 = kpiValue27;
	}
	public BigDecimal getKpiValue28() {
		return kpiValue28;
	}
	public void setKpiValue28(BigDecimal kpiValue28) {
		this.kpiValue28 = kpiValue28;
	}
	public BigDecimal getKpiValue29() {
		return kpiValue29;
	}
	public void setKpiValue29(BigDecimal kpiValue29) {
		this.kpiValue29 = kpiValue29;
	}
	public BigDecimal getKpiValue30() {
		return kpiValue30;
	}
	public void setKpiValue30(BigDecimal kpiValue30) {
		this.kpiValue30 = kpiValue30;
	}
	public BigDecimal getKpiValue31() {
		return kpiValue31;
	}
	public void setKpiValue31(BigDecimal kpiValue31) {
		this.kpiValue31 = kpiValue31;
	}
	public BigDecimal getKpiValue32() {
		return kpiValue32;
	}
	public void setKpiValue32(BigDecimal kpiValue32) {
		this.kpiValue32 = kpiValue32;
	}
	public BigDecimal getKpiValue33() {
		return kpiValue33;
	}
	public void setKpiValue33(BigDecimal kpiValue33) {
		this.kpiValue33 = kpiValue33;
	}
	public BigDecimal getKpiValue34() {
		return kpiValue34;
	}
	public void setKpiValue34(BigDecimal kpiValue34) {
		this.kpiValue34 = kpiValue34;
	}
	public BigDecimal getKpiValue35() {
		return kpiValue35;
	}
	public void setKpiValue35(BigDecimal kpiValue35) {
		this.kpiValue35 = kpiValue35;
	}
	public BigDecimal getKpiValue36() {
		return kpiValue36;
	}
	public void setKpiValue36(BigDecimal kpiValue36) {
		this.kpiValue36 = kpiValue36;
	}
	public BigDecimal getKpiValue37() {
		return kpiValue37;
	}
	public void setKpiValue37(BigDecimal kpiValue37) {
		this.kpiValue37 = kpiValue37;
	}
	public BigDecimal getKpiValue38() {
		return kpiValue38;
	}
	public void setKpiValue38(BigDecimal kpiValue38) {
		this.kpiValue38 = kpiValue38;
	}
	public BigDecimal getKpiValue39() {
		return kpiValue39;
	}
	public void setKpiValue39(BigDecimal kpiValue39) {
		this.kpiValue39 = kpiValue39;
	}
	public BigDecimal getKpiValue40() {
		return kpiValue40;
	}
	public void setKpiValue40(BigDecimal kpiValue40) {
		this.kpiValue40 = kpiValue40;
	}
	public Date getRegDtm() {
		return regDtm;
	}
	public void setRegDtm(Date regDtm) {
		this.regDtm = regDtm;
	}
	public String getPeNoReg() {
		return peNoReg;
	}
	public void setPeNoReg(String peNoReg) {
		this.peNoReg = peNoReg;
	}
	public Date getChgDtm() {
		return chgDtm;
	}
	public void setChgDtm(Date chgDtm) {
		this.chgDtm = chgDtm;
	}
	public String getPeNoChg() {
		return peNoChg;
	}
	public void setPeNoChg(String peNoChg) {
		this.peNoChg = peNoChg;
	}
	public String getSnapFlag() {
		return snapFlag;
	}
	public void setSnapFlag(String snapFlag) {
		this.snapFlag = snapFlag;
	}
	public Date getSnapDate() {
		return snapDate;
	}
	public void setSnapDate(Date snapDate) {
		this.snapDate = snapDate;
	}
    
}
