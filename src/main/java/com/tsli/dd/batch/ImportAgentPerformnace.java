package com.tsli.dd.batch;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.Transactional;

import com.tsli.dd.batch.constants.InqueryConstants.BatchSatus;
import com.tsli.dd.batch.constants.InqueryConstants.BatchType;
import com.tsli.dd.batch.entity.DdTAgentPerformance;
import com.tsli.dd.batch.entity.DdTAgentRankingPerformance;
import com.tsli.dd.batch.entity.DdTAgentStructure;
import com.tsli.dd.batch.entity.DdTClosCalendar;
import com.tsli.dd.batch.entity.DdTKpiMapping;
import com.tsli.dd.batch.entity.DdTKpiTarget;
import com.tsli.dd.batch.entity.StgScheduleLog;
import com.tsli.dd.batch.repository.TsliDDAgentPeroformanceRepository;
import com.tsli.dd.batch.service.ClosCalendarService;
import com.tsli.dd.batch.service.DatainService;
import com.tsli.dd.batch.service.TsliDDService;
import com.tsli.dd.batch.service.UsersService;

@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.tsli.dd" })
public class ImportAgentPerformnace extends AbstractBatch {

	@Autowired
	UsersService usersService;

	@Autowired
	DatainService datainService;

	@Autowired
	TsliDDService tsliDDService;

	@Autowired
	TsliDDAgentPeroformanceRepository tsliDDAgentPeroformanceRepository;

	@Autowired
	ClosCalendarService closCalendarService;

	DdTClosCalendar ddTClosCalendar = null;
	
	List<DdTClosCalendar> listDdTClosCalendar = new ArrayList<DdTClosCalendar>();
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(ImportAgentPerformnace.class, args);
	}

	@Override
	public void initail(String... args) throws Exception {
		batchCode = "IMPORT_AGENT_PERFORMANCE";
	}

	@Override
	public void start(String... args) throws Exception {

		super.setArgruments(args);

		StgScheduleLog batchDaily = closCalendarService.getBatchDaily();
		StgScheduleLog batchEndDay = closCalendarService.getBatchEndDay();

//		try {

//		} catch (Exception e) {
//			log.error("Cannot start batch import because STG_SCHEDULE_LOG data worng.");
//			log.error(e);
//		}

		try {
			
			if (!BatchSatus.DONE.equals(batchEndDay.getTsliStatus())) {
				log.warn("Cannot start import batch end day.");
				log.warn("Reason: Batch end day from TSLI source staging isn't success or error.");
				throw new Exception("Reason: Batch end day from TSLI source staging isn't success or error.");
			}
			if (!BatchSatus.DONE.equals(batchDaily.getTsliStatus()) && !BatchSatus.DONE.equals(batchDaily.getB3Status())
					&& BatchType.DAILY.equals(super.argBatchType)) {
				log.warn("Cannot start import batch daily.");
				log.warn("Reason: Batch daily from TSLI source staging isn't success or error.");
				throw new Exception("Reason: Batch daily from TSLI source staging isn't success or error.");
			}
			
			process();
		} catch (Exception e) {
			String msg = ("" + e.getMessage()).substring(0,
					e.getMessage().length() > 1999 ? 1999 : e.getMessage().length() - 1);
			closCalendarService.updateBatchStatus(super.startDate, new Date(), msg, BatchSatus.ERROR,
					super.argBatchType);
			log.error(e);
		}
	}

	@Transactional
	private void process() throws Exception {
		try {

			String calYM = null;

			if (argCalYM != null) {
				listDdTClosCalendar = closCalendarService.getListDdTClosCalendarByClosYM(argCalYM);
			} else if (argImportDate != null) {
				listDdTClosCalendar = closCalendarService.getListDdTClosCalendarByimportDate(argImportDate);
			} else {
			//	ddTClosCalendar = closCalendarService.getNearestClosYm();
				listDdTClosCalendar = closCalendarService.getListDdTClosCalendarByimportDate();
			}

			//if (ddTClosCalendar != null)
			//	calYM = ddTClosCalendar.getClosYm();

			log.info("argCalYM: " + argCalYM + " argImportDate: " + argImportDate + " calYM: " + calYM + " BatchId: "
					+ batchId);

			closCalendarService.updateBatchStatus(super.startDate, null, "", BatchSatus.DOING, super.argBatchType);
			for(DdTClosCalendar listClosYM: listDdTClosCalendar) {
				calYM = listClosYM.getClosYm();
				if (("" + batchId + "").contains("T1") || batchId == null) {
					List<DdTAgentPerformance> agentPerformancemodel = new ArrayList<DdTAgentPerformance>();
					agentPerformancemodel = datainService.getStatingAgentPerformance(calYM);
					tsliDDService.saveAgentPerformnaceModel(agentPerformancemodel, calYM);
				}
				
				if (("" + batchId + "").contains("T4") || batchId == null) {
					List<DdTAgentRankingPerformance> agentRankingPerformanceModel = new ArrayList<DdTAgentRankingPerformance>();
					agentRankingPerformanceModel = datainService.getAgentRankingPerformance(calYM);
					tsliDDService.saveAgentRankingPerformance(agentRankingPerformanceModel);
					log.info("AgentRankingPerformanceModel.size():" + agentRankingPerformanceModel.size());
				}

			}
			
			if (("" + batchId + "").contains("T2") || batchId == null) {
				List<DdTKpiMapping> kpiMappingModel = new ArrayList<DdTKpiMapping>();
				kpiMappingModel = datainService.getStatingKpiMapping(null);
				tsliDDService.saveKpiMapping(kpiMappingModel);
				log.info("kpiMappingModel.size():" + kpiMappingModel.size());
			}
			
			if (("" + batchId + "").contains("T3") || batchId == null) {
				List<DdTKpiTarget> kpiTargetModel = new ArrayList<DdTKpiTarget>();
				kpiTargetModel = datainService.getStatingKpiTarget(null);
				tsliDDService.saveKpiTarget(kpiTargetModel);
				log.info("kpiTargetModel.size():" + kpiTargetModel.size());
			}

			if (("" + batchId + "").contains("T5") || batchId == null) {
				List<DdTAgentStructure> agentStructureModel = new ArrayList<DdTAgentStructure>();
				agentStructureModel = datainService.getAgentStructure(null, null);
				tsliDDService.saveAgentStructure(agentStructureModel);
				log.info("AgentRankingPerformanceModel.size():" + agentStructureModel.size());
			}

			if (("" + batchId + "").contains("U1") || batchId == null) {
				usersService.importUser();
			}

			if( BatchType.END_DAY.equals(super.argBatchType) ) {
				closCalendarService.importClosCalendar();
				datainService.importLevelMapping();
				datainService.importZone();
			}
			
			closCalendarService.updateBatchStatus(super.startDate, new Date(), "", BatchSatus.DONE, super.argBatchType);
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			throw (e);
		}
	}
	
	
	
}
