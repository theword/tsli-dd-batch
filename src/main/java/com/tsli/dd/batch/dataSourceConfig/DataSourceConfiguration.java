package com.tsli.dd.batch.dataSourceConfig;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class DataSourceConfiguration {
//	@Autowired
//	private Environment env;
//
//	@Bean
//	@Primary
//	public DataSource firstDataSource()
//	{
//		DriverManagerDataSource dataSource = new DriverManagerDataSource();
//		dataSource.setDriverClassName(env.getProperty("stating.datasource.driver-class-name"));
//		dataSource.setUrl(env.getProperty("stating.datasource.url"));
//		dataSource.setUsername(env.getProperty("stating.datasource.username"));
//		dataSource.setPassword(env.getProperty("stating.datasource.password"));
//		return dataSource;
//	}
//
//	@Bean
//	public DataSource secondDataSource()
//	{
//		DriverManagerDataSource dataSource = new DriverManagerDataSource();
//		dataSource.setDriverClassName(env.getProperty("tslidd.datasource.driver-class-name"));
//		dataSource.setUrl(env.getProperty("tslidd.datasource.url"));
//		dataSource.setUsername(env.getProperty("tslidd.datasource.username"));
//		dataSource.setPassword(env.getProperty("tslidd.datasource.password"));
//		return dataSource;
//	}
//
//	@Bean
//	public JdbcTemplate jdbcTemplateOne(@Qualifier("firstDataSource") DataSource ds)
//	{
//		return new JdbcTemplate(ds);
//	}
//
//	@Bean
//	public JdbcTemplate jdbcTemplateTwo(@Qualifier("secondDataSource") DataSource ds)
//	{
//		return new JdbcTemplate(ds);
//	}
}
