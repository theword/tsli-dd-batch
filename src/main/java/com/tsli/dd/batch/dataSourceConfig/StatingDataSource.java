package com.tsli.dd.batch.dataSourceConfig;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.tsli.dd.batch.entity.DdTAgentPerformance;
import com.zaxxer.hikari.HikariDataSource;

//@Configuration
//@EnableTransactionManagement
//@EnableJpaRepositories(basePackages = "com.tsli.dd.batch.repository.TsliDDAgentPeroformanceRepository",
//        entityManagerFactoryRef = "tsliDDAgentPerformnaceEntityManagerFactory",
//        transactionManagerRef= "tsliDDAgentPerformnaceTransactionManager"
//)
public class StatingDataSource {
//    @Bean
//    @Primary
//    @ConfigurationProperties("app.datasource.tslidd")
//    public DataSourceProperties tsliDDAgentPerformnaceDataSourceProperties() {
//        return new DataSourceProperties();
//    }
//
//    @Bean
//    @Primary
//    @ConfigurationProperties("app.datasource.tslidd.configuration")
//    public DataSource tsliDDAgentPerformnaceDataSource() {
//        return tsliDDAgentPerformnaceDataSourceProperties().initializeDataSourceBuilder()
//                .type(HikariDataSource.class).build();
//    }
//
//    @Primary
//    @Bean(name = "tsliDDAgentPerformnaceEntityManagerFactory")
//    public LocalContainerEntityManagerFactoryBean tsliDDAgentPerformnaceEntityManagerFactory(EntityManagerFactoryBuilder builder) {
//        return builder
//                .dataSource(tsliDDAgentPerformnaceDataSource())
//                .packages(DdTAgentPerformance.class)
//                .build();
//    }
//
//    @Primary
//    @Bean
//    public PlatformTransactionManager tsliDDAgentPerformnaceTransactionManager(
//            final @Qualifier("tsliDDAgentPerformnaceEntityManagerFactory") LocalContainerEntityManagerFactoryBean tsliDDAgentPerformnaceEntityManagerFactory) {
//        return new JpaTransactionManager(tsliDDAgentPerformnaceEntityManagerFactory.getObject());
//    }

}
