package com.tsli.dd.batch;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.tsli.dd.batch.repository.StgScheduleLogRepository;
import com.tsli.dd.batch.service.ClosCalendarService;
import com.tsli.dd.batch.service.DatainService;



@EnableAsync
@ComponentScan("com.tsli.dd")
@EntityScan("com.tsli.dd.batch.entity")
@EnableJpaRepositories("com.tsli.dd.batch.repository")
@EnableTransactionManagement
@SpringBootApplication
public class DialyBatchApplication implements CommandLineRunner {

	@Value("lockengine.license.name")
	private String licenseName;
	private static final Logger log = LogManager.getLogger(DialyBatchApplication.class);
	@Autowired
	private StgScheduleLogRepository stgScheduleLogRepository;
	@PersistenceContext
	private EntityManager em;

	@Autowired
	ClosCalendarService closCalendarService;
	@Autowired
	DatainService datainService;
	
	public static void main(String[] args) {
		SpringApplication.run(DialyBatchApplication.class, args);
	}



	
	public void run(String... args) throws Exception {
		
//		closCalendarService.importClosCalendar();
		datainService.importLevelMapping();
		
//		stgScheduleLogRepository.findAll().forEach(new Consumer<Object>() {
//			public void accept(Object rows) {
//				log.info( rows);
//			}
//		});

		
//		ResAuth access = new ResAuth();
//		access.setUser(licenseName);
//		access.setPlatformInfo("14000");
//		ResAuthToken uuid = ResAuthToken.generateToken(access);
//		System.out.println(uuid.getToken() );
//		String code = ApiClient.decodeToken(uuid.getToken(), ResAuthToken.PROTOCOL);
//		System.out.println(code);
//		ResAuth.fromJsonString(code);
		
//		log.info("Hello");
//		
//		em.createNativeQuery("select * from Users").getResultList().forEach( new Consumer() {
//			public void accept(Object rows) {
//				log.info( ((Object[])rows)[0] + " " + ((Object[])rows)[1]);
//			}
//		} );
	}

}
