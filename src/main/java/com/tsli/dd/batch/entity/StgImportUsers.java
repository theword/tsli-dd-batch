package com.tsli.dd.batch.entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 */
@Entity
@Table(name = "STG_IMPORT_USERS", catalog = "", schema = "DATAHUB_PROD")
@NamedQueries({ @NamedQuery(name = "StgImportUsers.findAll", query = "SELECT s FROM StgImportUsers s"),
		@NamedQuery(name = "StgImportUsers.findById", query = "SELECT s FROM StgImportUsers s WHERE s.id = :id"),
		@NamedQuery(name = "StgImportUsers.findByUserName", query = "SELECT s FROM StgImportUsers s WHERE s.userName = :userName"),
		@NamedQuery(name = "StgImportUsers.findByUserType", query = "SELECT s FROM StgImportUsers s WHERE s.userType = :userType"),
		@NamedQuery(name = "StgImportUsers.findByName", query = "SELECT s FROM StgImportUsers s WHERE s.name = :name"),
		@NamedQuery(name = "StgImportUsers.findBySurname", query = "SELECT s FROM StgImportUsers s WHERE s.surname = :surname"),
		@NamedQuery(name = "StgImportUsers.findByStatus", query = "SELECT s FROM StgImportUsers s WHERE s.status = :status"),
		@NamedQuery(name = "StgImportUsers.findByTsliCreatedate", query = "SELECT s FROM StgImportUsers s WHERE s.tsliCreatedate = :tsliCreatedate"),
		@NamedQuery(name = "StgImportUsers.findByStatusToLockengine", query = "SELECT s FROM StgImportUsers s WHERE s.statusToLockengine = :statusToLockengine"),
		@NamedQuery(name = "StgImportUsers.findByB3Updatedate", query = "SELECT s FROM StgImportUsers s WHERE s.b3Updatedate = :b3Updatedate") })
public class StgImportUsers implements Serializable {

	private static final long serialVersionUID = 1L;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
	// consider using these annotations to enforce field validation
	@Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	// @Column(name = "ID")
	// private Long id;
	@Column(name = "USER_ID")
	private String userId;
	@Column(name = "USER_NAME")
	private String userName;
	@Column(name = "USER_TYPE")
	private String userType;
	@Column(name = "NAME")
	private String name;
	@Column(name = "SURNAME")
	private String surname;
	@Column(name = "STATUS")
	private Character status;
	@Column(name = "TSLI_CREATEDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tsliCreatedate;
	@Column(name = "STATUS_TO_LOCKENGINE")
	private String statusToLockengine;
	@Column(name = "B3_UPDATEDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date b3Updatedate;
	@Column(name = "REMARK", length=2000)
	private String remark;

	public StgImportUsers() {
	}

	public StgImportUsers(String userId) {
		this.userId = userId;
	}
	//
	// public Long getId() {
	// return id;
	// }
	//
	// public void setId(Long id) {
	// this.id = id;
	// }

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	public Date getTsliCreatedate() {
		return tsliCreatedate;
	}

	public void setTsliCreatedate(Date tsliCreatedate) {
		this.tsliCreatedate = tsliCreatedate;
	}

	public String getStatusToLockengine() {
		return statusToLockengine;
	}

	public void setStatusToLockengine(String statusToLockengine) {
		this.statusToLockengine = statusToLockengine;
	}

	public Date getB3Updatedate() {
		return b3Updatedate;
	}

	public void setB3Updatedate(Date b3Updatedate) {
		this.b3Updatedate = b3Updatedate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (userId != null ? userId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof StgImportUsers)) {
			return false;
		}
		StgImportUsers other = (StgImportUsers) object;
		if ((this.userId == null && other.userId != null)
				|| (this.userId != null && !this.userId.equals(other.userId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "StgImportUsers [userId=" + userId + ", userName=" + userName + ", userType=" + userType + ", name="
				+ name + ", surname=" + surname + ", status=" + status + ", tsliCreatedate=" + tsliCreatedate
				+ ", statusToLockengine=" + statusToLockengine + ", b3Updatedate=" + b3Updatedate + "]";
	}

}
