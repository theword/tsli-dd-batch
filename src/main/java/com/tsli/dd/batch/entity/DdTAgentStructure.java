/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsli.dd.batch.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "DD_T_AGENT_STRUCTURE", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DdTAgentStructure.findAll", query = "SELECT d FROM DdTAgentStructure d")
    , @NamedQuery(name = "DdTAgentStructure.findById", query = "SELECT d FROM DdTAgentStructure d WHERE d.id = :id")
    , @NamedQuery(name = "DdTAgentStructure.findByAgentCode", query = "SELECT d FROM DdTAgentStructure d WHERE d.agentCode = :agentCode")
    , @NamedQuery(name = "DdTAgentStructure.findByThaiTitleName", query = "SELECT d FROM DdTAgentStructure d WHERE d.thaiTitleName = :thaiTitleName")
    , @NamedQuery(name = "DdTAgentStructure.findByThaiName", query = "SELECT d FROM DdTAgentStructure d WHERE d.thaiName = :thaiName")
    , @NamedQuery(name = "DdTAgentStructure.findByThaiSurname", query = "SELECT d FROM DdTAgentStructure d WHERE d.thaiSurname = :thaiSurname")
    , @NamedQuery(name = "DdTAgentStructure.findByThaiFullName", query = "SELECT d FROM DdTAgentStructure d WHERE d.thaiFullName = :thaiFullName")
    , @NamedQuery(name = "DdTAgentStructure.findByEngTitleName", query = "SELECT d FROM DdTAgentStructure d WHERE d.engTitleName = :engTitleName")
    , @NamedQuery(name = "DdTAgentStructure.findByEngName", query = "SELECT d FROM DdTAgentStructure d WHERE d.engName = :engName")
    , @NamedQuery(name = "DdTAgentStructure.findByEngSurname", query = "SELECT d FROM DdTAgentStructure d WHERE d.engSurname = :engSurname")
    , @NamedQuery(name = "DdTAgentStructure.findByGradeCode", query = "SELECT d FROM DdTAgentStructure d WHERE d.gradeCode = :gradeCode")
    , @NamedQuery(name = "DdTAgentStructure.findByPosition", query = "SELECT d FROM DdTAgentStructure d WHERE d.position = :position")
    , @NamedQuery(name = "DdTAgentStructure.findByGradeOrder", query = "SELECT d FROM DdTAgentStructure d WHERE d.gradeOrder = :gradeOrder")
    , @NamedQuery(name = "DdTAgentStructure.findByAgentLevel", query = "SELECT d FROM DdTAgentStructure d WHERE d.agentLevel = :agentLevel")
    , @NamedQuery(name = "DdTAgentStructure.findByAgentStatusCode", query = "SELECT d FROM DdTAgentStructure d WHERE d.agentStatusCode = :agentStatusCode")
    , @NamedQuery(name = "DdTAgentStructure.findByStatusDesc", query = "SELECT d FROM DdTAgentStructure d WHERE d.statusDesc = :statusDesc")
    , @NamedQuery(name = "DdTAgentStructure.findByAgentSubstatusCode", query = "SELECT d FROM DdTAgentStructure d WHERE d.agentSubstatusCode = :agentSubstatusCode")
    , @NamedQuery(name = "DdTAgentStructure.findBySubstatusDesc", query = "SELECT d FROM DdTAgentStructure d WHERE d.substatusDesc = :substatusDesc")
    , @NamedQuery(name = "DdTAgentStructure.findByAppoinmentDate", query = "SELECT d FROM DdTAgentStructure d WHERE d.appoinmentDate = :appoinmentDate")
    , @NamedQuery(name = "DdTAgentStructure.findByWorkingMonths", query = "SELECT d FROM DdTAgentStructure d WHERE d.workingMonths = :workingMonths")
    , @NamedQuery(name = "DdTAgentStructure.findByStartWorkingDate", query = "SELECT d FROM DdTAgentStructure d WHERE d.startWorkingDate = :startWorkingDate")
    , @NamedQuery(name = "DdTAgentStructure.findByRetirementDate", query = "SELECT d FROM DdTAgentStructure d WHERE d.retirementDate = :retirementDate")
    , @NamedQuery(name = "DdTAgentStructure.findByGroupChannel", query = "SELECT d FROM DdTAgentStructure d WHERE d.groupChannel = :groupChannel")
    , @NamedQuery(name = "DdTAgentStructure.findByChannel", query = "SELECT d FROM DdTAgentStructure d WHERE d.channel = :channel")
    , @NamedQuery(name = "DdTAgentStructure.findByChannelName", query = "SELECT d FROM DdTAgentStructure d WHERE d.channelName = :channelName")
    , @NamedQuery(name = "DdTAgentStructure.findByZone", query = "SELECT d FROM DdTAgentStructure d WHERE d.zone = :zone")
    , @NamedQuery(name = "DdTAgentStructure.findByZoneCode", query = "SELECT d FROM DdTAgentStructure d WHERE d.zoneCode = :zoneCode")
    , @NamedQuery(name = "DdTAgentStructure.findByZoneName", query = "SELECT d FROM DdTAgentStructure d WHERE d.zoneName = :zoneName")
    , @NamedQuery(name = "DdTAgentStructure.findByAlCode", query = "SELECT d FROM DdTAgentStructure d WHERE d.alCode = :alCode")
    , @NamedQuery(name = "DdTAgentStructure.findByAvpCode", query = "SELECT d FROM DdTAgentStructure d WHERE d.avpCode = :avpCode")
    , @NamedQuery(name = "DdTAgentStructure.findByGmCode", query = "SELECT d FROM DdTAgentStructure d WHERE d.gmCode = :gmCode")
    , @NamedQuery(name = "DdTAgentStructure.findBySexDivisionCode", query = "SELECT d FROM DdTAgentStructure d WHERE d.sexDivisionCode = :sexDivisionCode")
    , @NamedQuery(name = "DdTAgentStructure.findByJobNature", query = "SELECT d FROM DdTAgentStructure d WHERE d.jobNature = :jobNature")
    , @NamedQuery(name = "DdTAgentStructure.findByDutyCode", query = "SELECT d FROM DdTAgentStructure d WHERE d.dutyCode = :dutyCode")
    , @NamedQuery(name = "DdTAgentStructure.findByDutyStartDate", query = "SELECT d FROM DdTAgentStructure d WHERE d.dutyStartDate = :dutyStartDate")
    , @NamedQuery(name = "DdTAgentStructure.findByEnterCompanyDate", query = "SELECT d FROM DdTAgentStructure d WHERE d.enterCompanyDate = :enterCompanyDate")
    , @NamedQuery(name = "DdTAgentStructure.findByTeamCode", query = "SELECT d FROM DdTAgentStructure d WHERE d.teamCode = :teamCode")
    , @NamedQuery(name = "DdTAgentStructure.findByOfficeCode", query = "SELECT d FROM DdTAgentStructure d WHERE d.officeCode = :officeCode")
    , @NamedQuery(name = "DdTAgentStructure.findByBranchCode", query = "SELECT d FROM DdTAgentStructure d WHERE d.branchCode = :branchCode")
    , @NamedQuery(name = "DdTAgentStructure.findByLicenseNumber", query = "SELECT d FROM DdTAgentStructure d WHERE d.licenseNumber = :licenseNumber")
    , @NamedQuery(name = "DdTAgentStructure.findByDateOfLicenseRenewal", query = "SELECT d FROM DdTAgentStructure d WHERE d.dateOfLicenseRenewal = :dateOfLicenseRenewal")
    , @NamedQuery(name = "DdTAgentStructure.findByDateOfLicenseExpiration", query = "SELECT d FROM DdTAgentStructure d WHERE d.dateOfLicenseExpiration = :dateOfLicenseExpiration")
    , @NamedQuery(name = "DdTAgentStructure.findByCarPark", query = "SELECT d FROM DdTAgentStructure d WHERE d.carPark = :carPark")
    , @NamedQuery(name = "DdTAgentStructure.findByTier1", query = "SELECT d FROM DdTAgentStructure d WHERE d.tier1 = :tier1")
    , @NamedQuery(name = "DdTAgentStructure.findByTier2", query = "SELECT d FROM DdTAgentStructure d WHERE d.tier2 = :tier2")
    , @NamedQuery(name = "DdTAgentStructure.findByTier3", query = "SELECT d FROM DdTAgentStructure d WHERE d.tier3 = :tier3")
    , @NamedQuery(name = "DdTAgentStructure.findByRecommenderCode", query = "SELECT d FROM DdTAgentStructure d WHERE d.recommenderCode = :recommenderCode")
    , @NamedQuery(name = "DdTAgentStructure.findByAgentCodeReference", query = "SELECT d FROM DdTAgentStructure d WHERE d.agentCodeReference = :agentCodeReference")
    , @NamedQuery(name = "DdTAgentStructure.findByDocRecvYmd", query = "SELECT d FROM DdTAgentStructure d WHERE d.docRecvYmd = :docRecvYmd")})
public class DdTAgentStructure implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, insertable = true, updatable = true)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "AGENT_CODE", nullable = false, length = 20)
    private String agentCode;
    @Column(name = "THAI_TITLE_NAME", length = 100)
    private String thaiTitleName;
    @Column(name = "THAI_NAME", length = 300)
    private String thaiName;
    @Column(name = "THAI_SURNAME", length = 300)
    private String thaiSurname;
    @Column(name = "THAI_FULL_NAME", length = 701)
    private String thaiFullName;
    @Column(name = "ENG_TITLE_NAME", length = 100)
    private String engTitleName;
    @Column(name = "ENG_NAME", length = 300)
    private String engName;
    @Column(name = "ENG_SURNAME", length = 100)
    private String engSurname;
    @Column(name = "GRADE_CODE", length = 2)
    private String gradeCode;
    @Column(name = "POSITION", length = 100)
    private String position;
    @Column(name = "GRADE_ORDER")
    private Integer gradeOrder;
    @Column(name = "AGENT_LEVEL", length = 5)
    private String agentLevel;
    @Column(name = "AGENT_STATUS_CODE")
    private short agentStatusCode;
    @Column(name = "STATUS_DESC", length = 30)
    private String statusDesc;
    @Column(name = "AGENT_SUBSTATUS_CODE")
    private Short agentSubstatusCode;
    @Column(name = "SUBSTATUS_DESC", length = 50)
    private String substatusDesc;
    @Column(name = "APPOINMENT_DATE", length = 20)
    private String appoinmentDate;
    @Column(name = "WORKING_MONTHS")
    private Short workingMonths;
    @Column(name = "START_WORKING_DATE", length = 20)
    private String startWorkingDate;
    @Column(name = "RETIREMENT_DATE", length = 20)
    private String retirementDate;
    @Column(name = "GROUP_CHANNEL", length = 20)
    private String groupChannel;
    @Column(name = "CHANNEL", length = 20)
    private String channel;
    @Column(name = "CHANNEL_NAME", length = 30)
    private String channelName;
    @Column(name = "ZONE", length = 20)
    private String zone;
    @Column(name = "ZONE_CODE", length = 20)
    private String zoneCode;
    @Column(name = "ZONE_NAME", length = 100)
    private String zoneName;
    @Column(name = "AL_CODE", length = 20)
    private String alCode;
    @Column(name = "AVP_CODE", length = 20)
    private String avpCode;
    @Column(name = "GM_CODE", length = 20)
    private String gmCode;
    @Column(name = "SEX_DIVISION_CODE")
    private Character sexDivisionCode;
    @Column(name = "JOB_NATURE", length = 20)
    private String jobNature;
    @Column(name = "DUTY_CODE")
    private Character dutyCode;
    @Column(name = "DUTY_START_DATE", length = 8)
    private String dutyStartDate;
    @Column(name = "ENTER_COMPANY_DATE", length = 8)
    private String enterCompanyDate;
    @Column(name = "TEAM_CODE")
    private Character teamCode;
    @Column(name = "OFFICE_CODE")
    private Character officeCode;
    @Column(name = "BRANCH_CODE", length = 50)
    private String branchCode;
    @Column(name = "LICENSE_NUMBER", length = 15)
    private String licenseNumber;
    @Column(name = "DATE_OF_LICENSE_RENEWAL", length = 8)
    private String dateOfLicenseRenewal;
    @Column(name = "DATE_OF_LICENSE_EXPIRATION", length = 8)
    private String dateOfLicenseExpiration;
    @Column(name = "CAR_PARK")
    private Character carPark;
    @Column(name = "TIER_1")
    private Character tier1;
    @Column(name = "TIER_2")
    private Character tier2;
    @Column(name = "TIER_3")
    private Character tier3;
    @Column(name = "RECOMMENDER_CODE", length = 20)
    private String recommenderCode;
    @Column(name = "AGENT_CODE_REFERENCE", length = 20)
    private String agentCodeReference;
    @Column(name = "DOC_RECV_YMD", length = 8)
    private String docRecvYmd;

    public DdTAgentStructure() {
    }

    public DdTAgentStructure(Integer id) {
        this.id = id;
    }

    public DdTAgentStructure(Integer id, String agentCode, short agentStatusCode, String channel, Character carPark) {
        this.id = id;
        this.agentCode = agentCode;
        this.agentStatusCode = agentStatusCode;
        this.channel = channel;
        this.carPark = carPark;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getThaiTitleName() {
        return thaiTitleName;
    }

    public void setThaiTitleName(String thaiTitleName) {
        this.thaiTitleName = thaiTitleName;
    }

    public String getThaiName() {
        return thaiName;
    }

    public void setThaiName(String thaiName) {
        this.thaiName = thaiName;
    }

    public String getThaiSurname() {
        return thaiSurname;
    }

    public void setThaiSurname(String thaiSurname) {
        this.thaiSurname = thaiSurname;
    }

    public String getThaiFullName() {
        return thaiFullName;
    }

    public void setThaiFullName(String thaiFullName) {
        this.thaiFullName = thaiFullName;
    }

    public String getEngTitleName() {
        return engTitleName;
    }

    public void setEngTitleName(String engTitleName) {
        this.engTitleName = engTitleName;
    }

    public String getEngName() {
        return engName;
    }

    public void setEngName(String engName) {
        this.engName = engName;
    }

    public String getEngSurname() {
        return engSurname;
    }

    public void setEngSurname(String engSurname) {
        this.engSurname = engSurname;
    }

    public String getGradeCode() {
        return gradeCode;
    }

    public void setGradeCode(String gradeCode) {
        this.gradeCode = gradeCode;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getGradeOrder() {
        return gradeOrder;
    }

    public void setGradeOrder(Integer gradeOrder) {
        this.gradeOrder = gradeOrder;
    }

    public String getAgentLevel() {
        return agentLevel;
    }

    public void setAgentLevel(String agentLevel) {
        this.agentLevel = agentLevel;
    }

    public short getAgentStatusCode() {
        return agentStatusCode;
    }

    public void setAgentStatusCode(short agentStatusCode) {
        this.agentStatusCode = agentStatusCode;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public Short getAgentSubstatusCode() {
        return agentSubstatusCode;
    }

    public void setAgentSubstatusCode(Short agentSubstatusCode) {
        this.agentSubstatusCode = agentSubstatusCode;
    }

    public String getSubstatusDesc() {
        return substatusDesc;
    }

    public void setSubstatusDesc(String substatusDesc) {
        this.substatusDesc = substatusDesc;
    }

    public String getAppoinmentDate() {
        return appoinmentDate;
    }

    public void setAppoinmentDate(String appoinmentDate) {
        this.appoinmentDate = appoinmentDate;
    }

    public Short getWorkingMonths() {
        return workingMonths;
    }

    public void setWorkingMonths(Short workingMonths) {
        this.workingMonths = workingMonths;
    }

    public String getStartWorkingDate() {
        return startWorkingDate;
    }

    public void setStartWorkingDate(String startWorkingDate) {
        this.startWorkingDate = startWorkingDate;
    }

    public String getRetirementDate() {
        return retirementDate;
    }

    public void setRetirementDate(String retirementDate) {
        this.retirementDate = retirementDate;
    }

    public String getGroupChannel() {
        return groupChannel;
    }

    public void setGroupChannel(String groupChannel) {
        this.groupChannel = groupChannel;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getZoneCode() {
        return zoneCode;
    }

    public void setZoneCode(String zoneCode) {
        this.zoneCode = zoneCode;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getAlCode() {
        return alCode;
    }

    public void setAlCode(String alCode) {
        this.alCode = alCode;
    }

    public String getAvpCode() {
        return avpCode;
    }

    public void setAvpCode(String avpCode) {
        this.avpCode = avpCode;
    }

    public String getGmCode() {
        return gmCode;
    }

    public void setGmCode(String gmCode) {
        this.gmCode = gmCode;
    }

    public Character getSexDivisionCode() {
        return sexDivisionCode;
    }

    public void setSexDivisionCode(Character sexDivisionCode) {
        this.sexDivisionCode = sexDivisionCode;
    }

    public String getJobNature() {
        return jobNature;
    }

    public void setJobNature(String jobNature) {
        this.jobNature = jobNature;
    }

    public Character getDutyCode() {
        return dutyCode;
    }

    public void setDutyCode(Character dutyCode) {
        this.dutyCode = dutyCode;
    }

    public String getDutyStartDate() {
        return dutyStartDate;
    }

    public void setDutyStartDate(String dutyStartDate) {
        this.dutyStartDate = dutyStartDate;
    }

    public String getEnterCompanyDate() {
        return enterCompanyDate;
    }

    public void setEnterCompanyDate(String enterCompanyDate) {
        this.enterCompanyDate = enterCompanyDate;
    }

    public Character getTeamCode() {
        return teamCode;
    }

    public void setTeamCode(Character teamCode) {
        this.teamCode = teamCode;
    }

    public Character getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(Character officeCode) {
        this.officeCode = officeCode;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getDateOfLicenseRenewal() {
        return dateOfLicenseRenewal;
    }

    public void setDateOfLicenseRenewal(String dateOfLicenseRenewal) {
        this.dateOfLicenseRenewal = dateOfLicenseRenewal;
    }

    public String getDateOfLicenseExpiration() {
        return dateOfLicenseExpiration;
    }

    public void setDateOfLicenseExpiration(String dateOfLicenseExpiration) {
        this.dateOfLicenseExpiration = dateOfLicenseExpiration;
    }

    public Character getCarPark() {
        return carPark;
    }

    public void setCarPark(Character carPark) {
        this.carPark = carPark;
    }

    public Character getTier1() {
        return tier1;
    }

    public void setTier1(Character tier1) {
        this.tier1 = tier1;
    }

    public Character getTier2() {
        return tier2;
    }

    public void setTier2(Character tier2) {
        this.tier2 = tier2;
    }

    public Character getTier3() {
        return tier3;
    }

    public void setTier3(Character tier3) {
        this.tier3 = tier3;
    }

    public String getRecommenderCode() {
        return recommenderCode;
    }

    public void setRecommenderCode(String recommenderCode) {
        this.recommenderCode = recommenderCode;
    }

    public String getAgentCodeReference() {
        return agentCodeReference;
    }

    public void setAgentCodeReference(String agentCodeReference) {
        this.agentCodeReference = agentCodeReference;
    }

    public String getDocRecvYmd() {
        return docRecvYmd;
    }

    public void setDocRecvYmd(String docRecvYmd) {
        this.docRecvYmd = docRecvYmd;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DdTAgentStructure)) {
            return false;
        }
        DdTAgentStructure other = (DdTAgentStructure) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tsli.dd.batch.entity.DdTAgentStructure[ id=" + id + " ]";
    }
    
}
