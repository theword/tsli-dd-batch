package com.tsli.dd.batch.entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 */
@Entity
@Table(name = "STG_SCHEDULE_LOG", catalog = "", schema = "DATAHUB_PROD")
public class StgScheduleLog implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@Column(name = "ID")
	private Integer id;
	@Column(name = "BATCH_TYPE", nullable= false)
	private Character batchType;
	@Column(name = "TSLI_STATUS")
	private Integer tsliStatus;
	@Column(name = "TSLI_START_DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tsliStartDate;
	@Column(name = "TSLI_END_DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tsliEndDate;
	@Column(name = "B3_STATUS")
	private Integer b3Status;
	@Column(name = "B3_START_DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date b3StartDate;
	@Column(name = "B3_END_DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date b3EndDate;
	@Column(name = "REMARK", length = 2000)
	private String remark;

	public StgScheduleLog() {
	}

	public StgScheduleLog(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Character getBatchType() {
		return batchType;
	}

	public void setBatchType(Character batchType) {
		this.batchType = batchType;
	}

	public Integer getTsliStatus() {
		return tsliStatus;
	}

	public void setTsliStatus(Integer tsliStatus) {
		this.tsliStatus = tsliStatus;
	}

	public Date getTsliStartDate() {
		return tsliStartDate;
	}

	public void setTsliStartDate(Date tsliStartDate) {
		this.tsliStartDate = tsliStartDate;
	}

	public Date getTsliEndDate() {
		return tsliEndDate;
	}

	public void setTsliEndDate(Date tsliEndDate) {
		this.tsliEndDate = tsliEndDate;
	}

	public Integer getB3Status() {
		return b3Status;
	}

	public void setB3Status(Integer b3Status) {
		this.b3Status = b3Status;
	}

	public Date getB3StartDate() {
		return b3StartDate;
	}

	public void setB3StartDate(Date b3StartDate) {
		this.b3StartDate = b3StartDate;
	}

	public Date getB3EndDate() {
		return b3EndDate;
	}

	public void setB3EndDate(Date b3EndDate) {
		this.b3EndDate = b3EndDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof StgScheduleLog)) {
			return false;
		}
		StgScheduleLog other = (StgScheduleLog) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "StgScheduleLog [id=" + id + ", batchType=" + batchType + ", tsliStatus=" + tsliStatus
				+ ", tsliStartDate=" + tsliStartDate + ", tsliEndDate=" + tsliEndDate + ", b3Status=" + b3Status
				+ ", b3StartDate=" + b3StartDate + ", b3EndDate=" + b3EndDate + ", remark=" + remark + "]";
	}

}
