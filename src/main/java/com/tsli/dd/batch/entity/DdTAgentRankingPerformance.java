/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsli.dd.batch.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author adisa
 */
@Entity
@Table(name = "DD_T_AGENT_RANKING_PERFORMANCE", catalog = "", schema = "")
@NamedQueries({
    @NamedQuery(name = "DdTAgentRankingPerformance.findAll", query = "SELECT d FROM DdTAgentRankingPerformance d"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findById", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.id = :id"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByClosYm", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.closYm = :closYm"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByKpiLevel", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.kpiLevel = :kpiLevel"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByCode", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.code = :code"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByCodeDescription", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.codeDescription = :codeDescription"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue1", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue1 = :rankKpiValue1"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue2", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue2 = :rankKpiValue2"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue3", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue3 = :rankKpiValue3"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue4", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue4 = :rankKpiValue4"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue5", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue5 = :rankKpiValue5"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue6", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue6 = :rankKpiValue6"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue7", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue7 = :rankKpiValue7"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue8", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue8 = :rankKpiValue8"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue9", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue9 = :rankKpiValue9"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue10", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue10 = :rankKpiValue10"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue11", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue11 = :rankKpiValue11"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue12", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue12 = :rankKpiValue12"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue13", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue13 = :rankKpiValue13"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue14", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue14 = :rankKpiValue14"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue15", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue15 = :rankKpiValue15"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue16", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue16 = :rankKpiValue16"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue17", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue17 = :rankKpiValue17"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue18", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue18 = :rankKpiValue18"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue19", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue19 = :rankKpiValue19"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue20", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue20 = :rankKpiValue20"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue21", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue21 = :rankKpiValue21"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue22", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue22 = :rankKpiValue22"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue23", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue23 = :rankKpiValue23"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue24", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue24 = :rankKpiValue24"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue25", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue25 = :rankKpiValue25"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue26", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue26 = :rankKpiValue26"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue27", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue27 = :rankKpiValue27"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue28", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue28 = :rankKpiValue28"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue29", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue29 = :rankKpiValue29"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue30", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue30 = :rankKpiValue30"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue31", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue31 = :rankKpiValue31"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue32", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue32 = :rankKpiValue32"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue33", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue33 = :rankKpiValue33"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue34", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue34 = :rankKpiValue34"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue35", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue35 = :rankKpiValue35"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue36", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue36 = :rankKpiValue36"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue37", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue37 = :rankKpiValue37"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue38", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue38 = :rankKpiValue38"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue39", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue39 = :rankKpiValue39"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findByRankKpiValue40", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.rankKpiValue40 = :rankKpiValue40"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue1", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue1 = :sumKpiValue1"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue2", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue2 = :sumKpiValue2"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue3", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue3 = :sumKpiValue3"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue4", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue4 = :sumKpiValue4"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue5", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue5 = :sumKpiValue5"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue6", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue6 = :sumKpiValue6"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue7", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue7 = :sumKpiValue7"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue8", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue8 = :sumKpiValue8"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue9", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue9 = :sumKpiValue9"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue10", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue10 = :sumKpiValue10"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue11", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue11 = :sumKpiValue11"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue12", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue12 = :sumKpiValue12"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue13", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue13 = :sumKpiValue13"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue14", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue14 = :sumKpiValue14"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue15", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue15 = :sumKpiValue15"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue16", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue16 = :sumKpiValue16"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue17", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue17 = :sumKpiValue17"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue18", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue18 = :sumKpiValue18"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue19", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue19 = :sumKpiValue19"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue20", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue20 = :sumKpiValue20"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue21", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue21 = :sumKpiValue21"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue22", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue22 = :sumKpiValue22"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue23", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue23 = :sumKpiValue23"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue24", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue24 = :sumKpiValue24"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue25", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue25 = :sumKpiValue25"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue26", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue26 = :sumKpiValue26"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue27", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue27 = :sumKpiValue27"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue28", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue28 = :sumKpiValue28"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue29", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue29 = :sumKpiValue29"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue30", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue30 = :sumKpiValue30"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue31", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue31 = :sumKpiValue31"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue32", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue32 = :sumKpiValue32"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue33", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue33 = :sumKpiValue33"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue34", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue34 = :sumKpiValue34"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue35", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue35 = :sumKpiValue35"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue36", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue36 = :sumKpiValue36"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue37", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue37 = :sumKpiValue37"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue38", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue38 = :sumKpiValue38"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue39", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue39 = :sumKpiValue39"),
    @NamedQuery(name = "DdTAgentRankingPerformance.findBySumKpiValue40", query = "SELECT d FROM DdTAgentRankingPerformance d WHERE d.sumKpiValue40 = :sumKpiValue40")})
public class DdTAgentRankingPerformance implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, insertable = true, updatable = true)
    private BigDecimal id;
    @Column(name = "CLOS_YM")
    private String closYm;
    @Column(name = "KPI_LEVEL")
    private String kpiLevel;
    @Column(name = "CODE")
    private String code;
    @Column(name = "CODE_DESCRIPTION")
    private String codeDescription;
    @Column(name = "RANK_KPI_VALUE_1")
    private BigDecimal rankKpiValue1;
    @Column(name = "RANK_KPI_VALUE_2")
    private BigDecimal rankKpiValue2;
    @Column(name = "RANK_KPI_VALUE_3")
    private BigDecimal rankKpiValue3;
    @Column(name = "RANK_KPI_VALUE_4")
    private BigDecimal rankKpiValue4;
    @Column(name = "RANK_KPI_VALUE_5")
    private BigDecimal rankKpiValue5;
    @Column(name = "RANK_KPI_VALUE_6")
    private BigDecimal rankKpiValue6;
    @Column(name = "RANK_KPI_VALUE_7")
    private BigDecimal rankKpiValue7;
    @Column(name = "RANK_KPI_VALUE_8")
    private BigDecimal rankKpiValue8;
    @Column(name = "RANK_KPI_VALUE_9")
    private BigDecimal rankKpiValue9;
    @Column(name = "RANK_KPI_VALUE_10")
    private BigDecimal rankKpiValue10;
    @Column(name = "RANK_KPI_VALUE_11")
    private BigDecimal rankKpiValue11;
    @Column(name = "RANK_KPI_VALUE_12")
    private BigDecimal rankKpiValue12;
    @Column(name = "RANK_KPI_VALUE_13")
    private BigDecimal rankKpiValue13;
    @Column(name = "RANK_KPI_VALUE_14")
    private BigDecimal rankKpiValue14;
    @Column(name = "RANK_KPI_VALUE_15")
    private BigDecimal rankKpiValue15;
    @Column(name = "RANK_KPI_VALUE_16")
    private BigDecimal rankKpiValue16;
    @Column(name = "RANK_KPI_VALUE_17")
    private BigDecimal rankKpiValue17;
    @Column(name = "RANK_KPI_VALUE_18")
    private BigDecimal rankKpiValue18;
    @Column(name = "RANK_KPI_VALUE_19")
    private BigDecimal rankKpiValue19;
    @Column(name = "RANK_KPI_VALUE_20")
    private BigDecimal rankKpiValue20;
    @Column(name = "RANK_KPI_VALUE_21")
    private BigDecimal rankKpiValue21;
    @Column(name = "RANK_KPI_VALUE_22")
    private BigDecimal rankKpiValue22;
    @Column(name = "RANK_KPI_VALUE_23")
    private BigDecimal rankKpiValue23;
    @Column(name = "RANK_KPI_VALUE_24")
    private BigDecimal rankKpiValue24;
    @Column(name = "RANK_KPI_VALUE_25")
    private BigDecimal rankKpiValue25;
    @Column(name = "RANK_KPI_VALUE_26")
    private BigDecimal rankKpiValue26;
    @Column(name = "RANK_KPI_VALUE_27")
    private BigDecimal rankKpiValue27;
    @Column(name = "RANK_KPI_VALUE_28")
    private BigDecimal rankKpiValue28;
    @Column(name = "RANK_KPI_VALUE_29")
    private BigDecimal rankKpiValue29;
    @Column(name = "RANK_KPI_VALUE_30")
    private BigDecimal rankKpiValue30;
    @Column(name = "RANK_KPI_VALUE_31")
    private BigDecimal rankKpiValue31;
    @Column(name = "RANK_KPI_VALUE_32")
    private BigDecimal rankKpiValue32;
    @Column(name = "RANK_KPI_VALUE_33")
    private BigDecimal rankKpiValue33;
    @Column(name = "RANK_KPI_VALUE_34")
    private BigDecimal rankKpiValue34;
    @Column(name = "RANK_KPI_VALUE_35")
    private BigDecimal rankKpiValue35;
    @Column(name = "RANK_KPI_VALUE_36")
    private BigDecimal rankKpiValue36;
    @Column(name = "RANK_KPI_VALUE_37")
    private BigDecimal rankKpiValue37;
    @Column(name = "RANK_KPI_VALUE_38")
    private BigDecimal rankKpiValue38;
    @Column(name = "RANK_KPI_VALUE_39")
    private BigDecimal rankKpiValue39;
    @Column(name = "RANK_KPI_VALUE_40")
    private BigDecimal rankKpiValue40;
    @Column(name = "SUM_KPI_VALUE_1")
    private BigDecimal sumKpiValue1;
    @Column(name = "SUM_KPI_VALUE_2")
    private BigDecimal sumKpiValue2;
    @Column(name = "SUM_KPI_VALUE_3")
    private BigDecimal sumKpiValue3;
    @Column(name = "SUM_KPI_VALUE_4")
    private BigDecimal sumKpiValue4;
    @Column(name = "SUM_KPI_VALUE_5")
    private BigDecimal sumKpiValue5;
    @Column(name = "SUM_KPI_VALUE_6")
    private BigDecimal sumKpiValue6;
    @Column(name = "SUM_KPI_VALUE_7")
    private BigDecimal sumKpiValue7;
    @Column(name = "SUM_KPI_VALUE_8")
    private BigDecimal sumKpiValue8;
    @Column(name = "SUM_KPI_VALUE_9")
    private BigDecimal sumKpiValue9;
    @Column(name = "SUM_KPI_VALUE_10")
    private BigDecimal sumKpiValue10;
    @Column(name = "SUM_KPI_VALUE_11")
    private BigDecimal sumKpiValue11;
    @Column(name = "SUM_KPI_VALUE_12")
    private BigDecimal sumKpiValue12;
    @Column(name = "SUM_KPI_VALUE_13")
    private BigDecimal sumKpiValue13;
    @Column(name = "SUM_KPI_VALUE_14")
    private BigDecimal sumKpiValue14;
    @Column(name = "SUM_KPI_VALUE_15")
    private BigDecimal sumKpiValue15;
    @Column(name = "SUM_KPI_VALUE_16")
    private BigDecimal sumKpiValue16;
    @Column(name = "SUM_KPI_VALUE_17")
    private BigDecimal sumKpiValue17;
    @Column(name = "SUM_KPI_VALUE_18")
    private BigDecimal sumKpiValue18;
    @Column(name = "SUM_KPI_VALUE_19")
    private BigDecimal sumKpiValue19;
    @Column(name = "SUM_KPI_VALUE_20")
    private BigDecimal sumKpiValue20;
    @Column(name = "SUM_KPI_VALUE_21")
    private BigDecimal sumKpiValue21;
    @Column(name = "SUM_KPI_VALUE_22")
    private BigDecimal sumKpiValue22;
    @Column(name = "SUM_KPI_VALUE_23")
    private BigDecimal sumKpiValue23;
    @Column(name = "SUM_KPI_VALUE_24")
    private BigDecimal sumKpiValue24;
    @Column(name = "SUM_KPI_VALUE_25")
    private BigDecimal sumKpiValue25;
    @Column(name = "SUM_KPI_VALUE_26")
    private BigDecimal sumKpiValue26;
    @Column(name = "SUM_KPI_VALUE_27")
    private BigDecimal sumKpiValue27;
    @Column(name = "SUM_KPI_VALUE_28")
    private BigDecimal sumKpiValue28;
    @Column(name = "SUM_KPI_VALUE_29")
    private BigDecimal sumKpiValue29;
    @Column(name = "SUM_KPI_VALUE_30")
    private BigDecimal sumKpiValue30;
    @Column(name = "SUM_KPI_VALUE_31")
    private BigDecimal sumKpiValue31;
    @Column(name = "SUM_KPI_VALUE_32")
    private BigDecimal sumKpiValue32;
    @Column(name = "SUM_KPI_VALUE_33")
    private BigDecimal sumKpiValue33;
    @Column(name = "SUM_KPI_VALUE_34")
    private BigDecimal sumKpiValue34;
    @Column(name = "SUM_KPI_VALUE_35")
    private BigDecimal sumKpiValue35;
    @Column(name = "SUM_KPI_VALUE_36")
    private BigDecimal sumKpiValue36;
    @Column(name = "SUM_KPI_VALUE_37")
    private BigDecimal sumKpiValue37;
    @Column(name = "SUM_KPI_VALUE_38")
    private BigDecimal sumKpiValue38;
    @Column(name = "SUM_KPI_VALUE_39")
    private BigDecimal sumKpiValue39;
    @Column(name = "SUM_KPI_VALUE_40")
    private BigDecimal sumKpiValue40;

    public DdTAgentRankingPerformance() {
    }

    public DdTAgentRankingPerformance(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getClosYm() {
        return closYm;
    }

    public void setClosYm(String closYm) {
        this.closYm = closYm;
    }

    public String getKpiLevel() {
        return kpiLevel;
    }

    public void setKpiLevel(String kpiLevel) {
        this.kpiLevel = kpiLevel;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDescription() {
        return codeDescription;
    }

    public void setCodeDescription(String codeDescription) {
        this.codeDescription = codeDescription;
    }

    public BigDecimal getRankKpiValue1() {
        return rankKpiValue1;
    }

    public void setRankKpiValue1(BigDecimal rankKpiValue1) {
        this.rankKpiValue1 = rankKpiValue1;
    }

    public BigDecimal getRankKpiValue2() {
        return rankKpiValue2;
    }

    public void setRankKpiValue2(BigDecimal rankKpiValue2) {
        this.rankKpiValue2 = rankKpiValue2;
    }

    public BigDecimal getRankKpiValue3() {
        return rankKpiValue3;
    }

    public void setRankKpiValue3(BigDecimal rankKpiValue3) {
        this.rankKpiValue3 = rankKpiValue3;
    }

    public BigDecimal getRankKpiValue4() {
        return rankKpiValue4;
    }

    public void setRankKpiValue4(BigDecimal rankKpiValue4) {
        this.rankKpiValue4 = rankKpiValue4;
    }

    public BigDecimal getRankKpiValue5() {
        return rankKpiValue5;
    }

    public void setRankKpiValue5(BigDecimal rankKpiValue5) {
        this.rankKpiValue5 = rankKpiValue5;
    }

    public BigDecimal getRankKpiValue6() {
        return rankKpiValue6;
    }

    public void setRankKpiValue6(BigDecimal bigDecimal) {
        this.rankKpiValue6 = bigDecimal;
    }

    public BigDecimal getRankKpiValue7() {
        return rankKpiValue7;
    }

    public void setRankKpiValue7(BigDecimal rankKpiValue7) {
        this.rankKpiValue7 = rankKpiValue7;
    }

    public BigDecimal getRankKpiValue8() {
        return rankKpiValue8;
    }

    public void setRankKpiValue8(BigDecimal rankKpiValue8) {
        this.rankKpiValue8 = rankKpiValue8;
    }

    public BigDecimal getRankKpiValue9() {
        return rankKpiValue9;
    }

    public void setRankKpiValue9(BigDecimal rankKpiValue9) {
        this.rankKpiValue9 = rankKpiValue9;
    }

    public BigDecimal getRankKpiValue10() {
        return rankKpiValue10;
    }

    public void setRankKpiValue10(BigDecimal rankKpiValue10) {
        this.rankKpiValue10 = rankKpiValue10;
    }

    public BigDecimal getRankKpiValue11() {
        return rankKpiValue11;
    }

    public void setRankKpiValue11(BigDecimal rankKpiValue11) {
        this.rankKpiValue11 = rankKpiValue11;
    }

    public BigDecimal getRankKpiValue12() {
        return rankKpiValue12;
    }

    public void setRankKpiValue12(BigDecimal rankKpiValue12) {
        this.rankKpiValue12 = rankKpiValue12;
    }

    public BigDecimal getRankKpiValue13() {
        return rankKpiValue13;
    }

    public void setRankKpiValue13(BigDecimal rankKpiValue13) {
        this.rankKpiValue13 = rankKpiValue13;
    }

    public BigDecimal getRankKpiValue14() {
        return rankKpiValue14;
    }

    public void setRankKpiValue14(BigDecimal rankKpiValue14) {
        this.rankKpiValue14 = rankKpiValue14;
    }

    public BigDecimal getRankKpiValue15() {
        return rankKpiValue15;
    }

    public void setRankKpiValue15(BigDecimal rankKpiValue15) {
        this.rankKpiValue15 = rankKpiValue15;
    }

    public BigDecimal getRankKpiValue16() {
        return rankKpiValue16;
    }

    public void setRankKpiValue16(BigDecimal rankKpiValue16) {
        this.rankKpiValue16 = rankKpiValue16;
    }

    public BigDecimal getRankKpiValue17() {
        return rankKpiValue17;
    }

    public void setRankKpiValue17(BigDecimal rankKpiValue17) {
        this.rankKpiValue17 = rankKpiValue17;
    }

    public BigDecimal getRankKpiValue18() {
        return rankKpiValue18;
    }

    public void setRankKpiValue18(BigDecimal rankKpiValue18) {
        this.rankKpiValue18 = rankKpiValue18;
    }

    public BigDecimal getRankKpiValue19() {
        return rankKpiValue19;
    }

    public void setRankKpiValue19(BigDecimal rankKpiValue19) {
        this.rankKpiValue19 = rankKpiValue19;
    }

    public BigDecimal getRankKpiValue20() {
        return rankKpiValue20;
    }

    public void setRankKpiValue20(BigDecimal rankKpiValue20) {
        this.rankKpiValue20 = rankKpiValue20;
    }

    public BigDecimal getRankKpiValue21() {
        return rankKpiValue21;
    }

    public void setRankKpiValue21(BigDecimal rankKpiValue21) {
        this.rankKpiValue21 = rankKpiValue21;
    }

    public BigDecimal getRankKpiValue22() {
        return rankKpiValue22;
    }

    public void setRankKpiValue22(BigDecimal rankKpiValue22) {
        this.rankKpiValue22 = rankKpiValue22;
    }

    public BigDecimal getRankKpiValue23() {
        return rankKpiValue23;
    }

    public void setRankKpiValue23(BigDecimal rankKpiValue23) {
        this.rankKpiValue23 = rankKpiValue23;
    }

    public BigDecimal getRankKpiValue24() {
        return rankKpiValue24;
    }

    public void setRankKpiValue24(BigDecimal rankKpiValue24) {
        this.rankKpiValue24 = rankKpiValue24;
    }

    public BigDecimal getRankKpiValue25() {
        return rankKpiValue25;
    }

    public void setRankKpiValue25(BigDecimal rankKpiValue25) {
        this.rankKpiValue25 = rankKpiValue25;
    }

    public BigDecimal getRankKpiValue26() {
        return rankKpiValue26;
    }

    public void setRankKpiValue26(BigDecimal rankKpiValue26) {
        this.rankKpiValue26 = rankKpiValue26;
    }

    public BigDecimal getRankKpiValue27() {
        return rankKpiValue27;
    }

    public void setRankKpiValue27(BigDecimal rankKpiValue27) {
        this.rankKpiValue27 = rankKpiValue27;
    }

    public BigDecimal getRankKpiValue28() {
        return rankKpiValue28;
    }

    public void setRankKpiValue28(BigDecimal rankKpiValue28) {
        this.rankKpiValue28 = rankKpiValue28;
    }

    public BigDecimal getRankKpiValue29() {
        return rankKpiValue29;
    }

    public void setRankKpiValue29(BigDecimal rankKpiValue29) {
        this.rankKpiValue29 = rankKpiValue29;
    }

    public BigDecimal getRankKpiValue30() {
        return rankKpiValue30;
    }

    public void setRankKpiValue30(BigDecimal rankKpiValue30) {
        this.rankKpiValue30 = rankKpiValue30;
    }

    public BigDecimal getRankKpiValue31() {
        return rankKpiValue31;
    }

    public void setRankKpiValue31(BigDecimal rankKpiValue31) {
        this.rankKpiValue31 = rankKpiValue31;
    }

    public BigDecimal getRankKpiValue32() {
        return rankKpiValue32;
    }

    public void setRankKpiValue32(BigDecimal rankKpiValue32) {
        this.rankKpiValue32 = rankKpiValue32;
    }

    public BigDecimal getRankKpiValue33() {
        return rankKpiValue33;
    }

    public void setRankKpiValue33(BigDecimal rankKpiValue33) {
        this.rankKpiValue33 = rankKpiValue33;
    }

    public BigDecimal getRankKpiValue34() {
        return rankKpiValue34;
    }

    public void setRankKpiValue34(BigDecimal rankKpiValue34) {
        this.rankKpiValue34 = rankKpiValue34;
    }

    public BigDecimal getRankKpiValue35() {
        return rankKpiValue35;
    }

    public void setRankKpiValue35(BigDecimal rankKpiValue35) {
        this.rankKpiValue35 = rankKpiValue35;
    }

    public BigDecimal getRankKpiValue36() {
        return rankKpiValue36;
    }

    public void setRankKpiValue36(BigDecimal rankKpiValue36) {
        this.rankKpiValue36 = rankKpiValue36;
    }

    public BigDecimal getRankKpiValue37() {
        return rankKpiValue37;
    }

    public void setRankKpiValue37(BigDecimal rankKpiValue37) {
        this.rankKpiValue37 = rankKpiValue37;
    }

    public BigDecimal getRankKpiValue38() {
        return rankKpiValue38;
    }

    public void setRankKpiValue38(BigDecimal rankKpiValue38) {
        this.rankKpiValue38 = rankKpiValue38;
    }

    public BigDecimal getRankKpiValue39() {
        return rankKpiValue39;
    }

    public void setRankKpiValue39(BigDecimal rankKpiValue39) {
        this.rankKpiValue39 = rankKpiValue39;
    }

    public BigDecimal getRankKpiValue40() {
        return rankKpiValue40;
    }

    public void setRankKpiValue40(BigDecimal rankKpiValue40) {
        this.rankKpiValue40 = rankKpiValue40;
    }

    public BigDecimal getSumKpiValue1() {
        return sumKpiValue1;
    }

    public void setSumKpiValue1(BigDecimal sumKpiValue1) {
        this.sumKpiValue1 = sumKpiValue1;
    }

    public BigDecimal getSumKpiValue2() {
        return sumKpiValue2;
    }

    public void setSumKpiValue2(BigDecimal sumKpiValue2) {
        this.sumKpiValue2 = sumKpiValue2;
    }

    public BigDecimal getSumKpiValue3() {
        return sumKpiValue3;
    }

    public void setSumKpiValue3(BigDecimal sumKpiValue3) {
        this.sumKpiValue3 = sumKpiValue3;
    }

    public BigDecimal getSumKpiValue4() {
        return sumKpiValue4;
    }

    public void setSumKpiValue4(BigDecimal sumKpiValue4) {
        this.sumKpiValue4 = sumKpiValue4;
    }

    public BigDecimal getSumKpiValue5() {
        return sumKpiValue5;
    }

    public void setSumKpiValue5(BigDecimal sumKpiValue5) {
        this.sumKpiValue5 = sumKpiValue5;
    }

    public BigDecimal getSumKpiValue6() {
        return sumKpiValue6;
    }

    public void setSumKpiValue6(BigDecimal sumKpiValue6) {
        this.sumKpiValue6 = sumKpiValue6;
    }

    public BigDecimal getSumKpiValue7() {
        return sumKpiValue7;
    }

    public void setSumKpiValue7(BigDecimal sumKpiValue7) {
        this.sumKpiValue7 = sumKpiValue7;
    }

    public BigDecimal getSumKpiValue8() {
        return sumKpiValue8;
    }

    public void setSumKpiValue8(BigDecimal sumKpiValue8) {
        this.sumKpiValue8 = sumKpiValue8;
    }

    public BigDecimal getSumKpiValue9() {
        return sumKpiValue9;
    }

    public void setSumKpiValue9(BigDecimal sumKpiValue9) {
        this.sumKpiValue9 = sumKpiValue9;
    }

    public BigDecimal getSumKpiValue10() {
        return sumKpiValue10;
    }

    public void setSumKpiValue10(BigDecimal sumKpiValue10) {
        this.sumKpiValue10 = sumKpiValue10;
    }

    public BigDecimal getSumKpiValue11() {
        return sumKpiValue11;
    }

    public void setSumKpiValue11(BigDecimal sumKpiValue11) {
        this.sumKpiValue11 = sumKpiValue11;
    }

    public BigDecimal getSumKpiValue12() {
        return sumKpiValue12;
    }

    public void setSumKpiValue12(BigDecimal sumKpiValue12) {
        this.sumKpiValue12 = sumKpiValue12;
    }

    public BigDecimal getSumKpiValue13() {
        return sumKpiValue13;
    }

    public void setSumKpiValue13(BigDecimal sumKpiValue13) {
        this.sumKpiValue13 = sumKpiValue13;
    }

    public BigDecimal getSumKpiValue14() {
        return sumKpiValue14;
    }

    public void setSumKpiValue14(BigDecimal sumKpiValue14) {
        this.sumKpiValue14 = sumKpiValue14;
    }

    public BigDecimal getSumKpiValue15() {
        return sumKpiValue15;
    }

    public void setSumKpiValue15(BigDecimal sumKpiValue15) {
        this.sumKpiValue15 = sumKpiValue15;
    }

    public BigDecimal getSumKpiValue16() {
        return sumKpiValue16;
    }

    public void setSumKpiValue16(BigDecimal sumKpiValue16) {
        this.sumKpiValue16 = sumKpiValue16;
    }

    public BigDecimal getSumKpiValue17() {
        return sumKpiValue17;
    }

    public void setSumKpiValue17(BigDecimal sumKpiValue17) {
        this.sumKpiValue17 = sumKpiValue17;
    }

    public BigDecimal getSumKpiValue18() {
        return sumKpiValue18;
    }

    public void setSumKpiValue18(BigDecimal sumKpiValue18) {
        this.sumKpiValue18 = sumKpiValue18;
    }

    public BigDecimal getSumKpiValue19() {
        return sumKpiValue19;
    }

    public void setSumKpiValue19(BigDecimal sumKpiValue19) {
        this.sumKpiValue19 = sumKpiValue19;
    }

    public BigDecimal getSumKpiValue20() {
        return sumKpiValue20;
    }

    public void setSumKpiValue20(BigDecimal sumKpiValue20) {
        this.sumKpiValue20 = sumKpiValue20;
    }

    public BigDecimal getSumKpiValue21() {
        return sumKpiValue21;
    }

    public void setSumKpiValue21(BigDecimal sumKpiValue21) {
        this.sumKpiValue21 = sumKpiValue21;
    }

    public BigDecimal getSumKpiValue22() {
        return sumKpiValue22;
    }

    public void setSumKpiValue22(BigDecimal sumKpiValue22) {
        this.sumKpiValue22 = sumKpiValue22;
    }

    public BigDecimal getSumKpiValue23() {
        return sumKpiValue23;
    }

    public void setSumKpiValue23(BigDecimal sumKpiValue23) {
        this.sumKpiValue23 = sumKpiValue23;
    }

    public BigDecimal getSumKpiValue24() {
        return sumKpiValue24;
    }

    public void setSumKpiValue24(BigDecimal sumKpiValue24) {
        this.sumKpiValue24 = sumKpiValue24;
    }

    public BigDecimal getSumKpiValue25() {
        return sumKpiValue25;
    }

    public void setSumKpiValue25(BigDecimal sumKpiValue25) {
        this.sumKpiValue25 = sumKpiValue25;
    }

    public BigDecimal getSumKpiValue26() {
        return sumKpiValue26;
    }

    public void setSumKpiValue26(BigDecimal sumKpiValue26) {
        this.sumKpiValue26 = sumKpiValue26;
    }

    public BigDecimal getSumKpiValue27() {
        return sumKpiValue27;
    }

    public void setSumKpiValue27(BigDecimal sumKpiValue27) {
        this.sumKpiValue27 = sumKpiValue27;
    }

    public BigDecimal getSumKpiValue28() {
        return sumKpiValue28;
    }

    public void setSumKpiValue28(BigDecimal sumKpiValue28) {
        this.sumKpiValue28 = sumKpiValue28;
    }

    public BigDecimal getSumKpiValue29() {
        return sumKpiValue29;
    }

    public void setSumKpiValue29(BigDecimal sumKpiValue29) {
        this.sumKpiValue29 = sumKpiValue29;
    }

    public BigDecimal getSumKpiValue30() {
        return sumKpiValue30;
    }

    public void setSumKpiValue30(BigDecimal sumKpiValue30) {
        this.sumKpiValue30 = sumKpiValue30;
    }

    public BigDecimal getSumKpiValue31() {
        return sumKpiValue31;
    }

    public void setSumKpiValue31(BigDecimal sumKpiValue31) {
        this.sumKpiValue31 = sumKpiValue31;
    }

    public BigDecimal getSumKpiValue32() {
        return sumKpiValue32;
    }

    public void setSumKpiValue32(BigDecimal sumKpiValue32) {
        this.sumKpiValue32 = sumKpiValue32;
    }

    public BigDecimal getSumKpiValue33() {
        return sumKpiValue33;
    }

    public void setSumKpiValue33(BigDecimal sumKpiValue33) {
        this.sumKpiValue33 = sumKpiValue33;
    }

    public BigDecimal getSumKpiValue34() {
        return sumKpiValue34;
    }

    public void setSumKpiValue34(BigDecimal sumKpiValue34) {
        this.sumKpiValue34 = sumKpiValue34;
    }

    public BigDecimal getSumKpiValue35() {
        return sumKpiValue35;
    }

    public void setSumKpiValue35(BigDecimal sumKpiValue35) {
        this.sumKpiValue35 = sumKpiValue35;
    }

    public BigDecimal getSumKpiValue36() {
        return sumKpiValue36;
    }

    public void setSumKpiValue36(BigDecimal sumKpiValue36) {
        this.sumKpiValue36 = sumKpiValue36;
    }

    public BigDecimal getSumKpiValue37() {
        return sumKpiValue37;
    }

    public void setSumKpiValue37(BigDecimal sumKpiValue37) {
        this.sumKpiValue37 = sumKpiValue37;
    }

    public BigDecimal getSumKpiValue38() {
        return sumKpiValue38;
    }

    public void setSumKpiValue38(BigDecimal sumKpiValue38) {
        this.sumKpiValue38 = sumKpiValue38;
    }

    public BigDecimal getSumKpiValue39() {
        return sumKpiValue39;
    }

    public void setSumKpiValue39(BigDecimal sumKpiValue39) {
        this.sumKpiValue39 = sumKpiValue39;
    }

    public BigDecimal getSumKpiValue40() {
        return sumKpiValue40;
    }

    public void setSumKpiValue40(BigDecimal sumKpiValue40) {
        this.sumKpiValue40 = sumKpiValue40;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DdTAgentRankingPerformance)) {
            return false;
        }
        DdTAgentRankingPerformance other = (DdTAgentRankingPerformance) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
	public String toString() {
		return "DdTAgentRankingPerformance [id=" + id + ", closYm=" + closYm + ", kpiLevel=" + kpiLevel + ", code="
				+ code + ", codeDescription=" + codeDescription + ", rankKpiValue1=" + rankKpiValue1
				+ ", rankKpiValue2=" + rankKpiValue2 + ", rankKpiValue3=" + rankKpiValue3 + ", rankKpiValue4="
				+ rankKpiValue4 + ", rankKpiValue5=" + rankKpiValue5 + ", rankKpiValue6=" + rankKpiValue6
				+ ", rankKpiValue7=" + rankKpiValue7 + ", rankKpiValue8=" + rankKpiValue8 + ", rankKpiValue9="
				+ rankKpiValue9 + ", rankKpiValue10=" + rankKpiValue10 + ", rankKpiValue11=" + rankKpiValue11
				+ ", rankKpiValue12=" + rankKpiValue12 + ", rankKpiValue13=" + rankKpiValue13 + ", rankKpiValue14="
				+ rankKpiValue14 + ", rankKpiValue15=" + rankKpiValue15 + ", rankKpiValue16=" + rankKpiValue16
				+ ", rankKpiValue17=" + rankKpiValue17 + ", rankKpiValue18=" + rankKpiValue18 + ", rankKpiValue19="
				+ rankKpiValue19 + ", rankKpiValue20=" + rankKpiValue20 + ", rankKpiValue21=" + rankKpiValue21
				+ ", rankKpiValue22=" + rankKpiValue22 + ", rankKpiValue23=" + rankKpiValue23 + ", rankKpiValue24="
				+ rankKpiValue24 + ", rankKpiValue25=" + rankKpiValue25 + ", rankKpiValue26=" + rankKpiValue26
				+ ", rankKpiValue27=" + rankKpiValue27 + ", rankKpiValue28=" + rankKpiValue28 + ", rankKpiValue29="
				+ rankKpiValue29 + ", rankKpiValue30=" + rankKpiValue30 + ", rankKpiValue31=" + rankKpiValue31
				+ ", rankKpiValue32=" + rankKpiValue32 + ", rankKpiValue33=" + rankKpiValue33 + ", rankKpiValue34="
				+ rankKpiValue34 + ", rankKpiValue35=" + rankKpiValue35 + ", rankKpiValue36=" + rankKpiValue36
				+ ", rankKpiValue37=" + rankKpiValue37 + ", rankKpiValue38=" + rankKpiValue38 + ", rankKpiValue39="
				+ rankKpiValue39 + ", rankKpiValue40=" + rankKpiValue40 + ", sumKpiValue1=" + sumKpiValue1
				+ ", sumKpiValue2=" + sumKpiValue2 + ", sumKpiValue3=" + sumKpiValue3 + ", sumKpiValue4=" + sumKpiValue4
				+ ", sumKpiValue5=" + sumKpiValue5 + ", sumKpiValue6=" + sumKpiValue6 + ", sumKpiValue7=" + sumKpiValue7
				+ ", sumKpiValue8=" + sumKpiValue8 + ", sumKpiValue9=" + sumKpiValue9 + ", sumKpiValue10="
				+ sumKpiValue10 + ", sumKpiValue11=" + sumKpiValue11 + ", sumKpiValue12=" + sumKpiValue12
				+ ", sumKpiValue13=" + sumKpiValue13 + ", sumKpiValue14=" + sumKpiValue14 + ", sumKpiValue15="
				+ sumKpiValue15 + ", sumKpiValue16=" + sumKpiValue16 + ", sumKpiValue17=" + sumKpiValue17
				+ ", sumKpiValue18=" + sumKpiValue18 + ", sumKpiValue19=" + sumKpiValue19 + ", sumKpiValue20="
				+ sumKpiValue20 + ", sumKpiValue21=" + sumKpiValue21 + ", sumKpiValue22=" + sumKpiValue22
				+ ", sumKpiValue23=" + sumKpiValue23 + ", sumKpiValue24=" + sumKpiValue24 + ", sumKpiValue25="
				+ sumKpiValue25 + ", sumKpiValue26=" + sumKpiValue26 + ", sumKpiValue27=" + sumKpiValue27
				+ ", sumKpiValue28=" + sumKpiValue28 + ", sumKpiValue29=" + sumKpiValue29 + ", sumKpiValue30="
				+ sumKpiValue30 + ", sumKpiValue31=" + sumKpiValue31 + ", sumKpiValue32=" + sumKpiValue32
				+ ", sumKpiValue33=" + sumKpiValue33 + ", sumKpiValue34=" + sumKpiValue34 + ", sumKpiValue35="
				+ sumKpiValue35 + ", sumKpiValue36=" + sumKpiValue36 + ", sumKpiValue37=" + sumKpiValue37
				+ ", sumKpiValue38=" + sumKpiValue38 + ", sumKpiValue39=" + sumKpiValue39 + ", sumKpiValue40="
				+ sumKpiValue40 + "]";
	}
    
}

   
