package com.tsli.dd.batch.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author adisa
 */
@Entity
@Table(name = "DD_T_CLOS_CALENDAR", catalog = "", schema = "")
@NamedQueries({
    @NamedQuery(name = "DdTClosCalendar.findAll", query = "SELECT d FROM DdTClosCalendar d"),
    @NamedQuery(name = "DdTClosCalendar.findById", query = "SELECT d FROM DdTClosCalendar d WHERE d.id = :id"),
    @NamedQuery(name = "DdTClosCalendar.findByClosYm", query = "SELECT d FROM DdTClosCalendar d WHERE d.closYm = :closYm"),
    @NamedQuery(name = "DdTClosCalendar.findByPrStrYmd", query = "SELECT d FROM DdTClosCalendar d WHERE d.prStrYmd = :prStrYmd"),
    @NamedQuery(name = "DdTClosCalendar.findByPrEndYmd", query = "SELECT d FROM DdTClosCalendar d WHERE d.prEndYmd = :prEndYmd"),
    @NamedQuery(name = "DdTClosCalendar.findByUwStrYmd", query = "SELECT d FROM DdTClosCalendar d WHERE d.uwStrYmd = :uwStrYmd"),
    @NamedQuery(name = "DdTClosCalendar.findByUwEndYmd", query = "SELECT d FROM DdTClosCalendar d WHERE d.uwEndYmd = :uwEndYmd"),
    @NamedQuery(name = "DdTClosCalendar.findByCalendarStrYmd", query = "SELECT d FROM DdTClosCalendar d WHERE d.calendarStrYmd = :calendarStrYmd"),
    @NamedQuery(name = "DdTClosCalendar.findByCalendarEndYmd", query = "SELECT d FROM DdTClosCalendar d WHERE d.calendarEndYmd = :calendarEndYmd"),
    @NamedQuery(name = "DdTClosCalendar.findByFlagEndClosingYmd", query = "SELECT d FROM DdTClosCalendar d WHERE d.flagEndClosingYmd = :flagEndClosingYmd"),
    @NamedQuery(name = "DdTClosCalendar.findByEndClosingYmd", query = "SELECT d FROM DdTClosCalendar d WHERE d.endClosingYmd = :endClosingYmd")})
public class DdTClosCalendar implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, insertable = true, updatable = true)
    private BigDecimal id;
    @Column(name = "CLOS_YM")
    private String closYm;
    @Column(name = "PR_STR_YMD")
    private String prStrYmd;
    @Column(name = "PR_END_YMD")
    private String prEndYmd;
    @Column(name = "UW_STR_YMD")
    private String uwStrYmd;
    @Column(name = "UW_END_YMD")
    private String uwEndYmd;
    @Column(name = "CALENDAR_STR_YMD")
    private String calendarStrYmd;
    @Column(name = "CALENDAR_END_YMD")
    private String calendarEndYmd;
    @Column(name = "FLAG_END_CLOSING_YMD")
    private String flagEndClosingYmd;
    @Column(name = "END_CLOSING_YMD")
    private String endClosingYmd;

    public DdTClosCalendar() {
    }

    public DdTClosCalendar(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getClosYm() {
        return closYm;
    }

    public void setClosYm(String closYm) {
        this.closYm = closYm;
    }

    public String getPrStrYmd() {
        return prStrYmd;
    }

    public void setPrStrYmd(String prStrYmd) {
        this.prStrYmd = prStrYmd;
    }

    public String getPrEndYmd() {
        return prEndYmd;
    }

    public void setPrEndYmd(String prEndYmd) {
        this.prEndYmd = prEndYmd;
    }

    public String getUwStrYmd() {
        return uwStrYmd;
    }

    public void setUwStrYmd(String uwStrYmd) {
        this.uwStrYmd = uwStrYmd;
    }

    public String getUwEndYmd() {
        return uwEndYmd;
    }

    public void setUwEndYmd(String uwEndYmd) {
        this.uwEndYmd = uwEndYmd;
    }

    public String getCalendarStrYmd() {
        return calendarStrYmd;
    }

    public void setCalendarStrYmd(String calendarStrYmd) {
        this.calendarStrYmd = calendarStrYmd;
    }

    public String getCalendarEndYmd() {
        return calendarEndYmd;
    }

    public void setCalendarEndYmd(String calendarEndYmd) {
        this.calendarEndYmd = calendarEndYmd;
    }

    public String getFlagEndClosingYmd() {
        return flagEndClosingYmd;
    }

    public void setFlagEndClosingYmd(String flagEndClosingYmd) {
        this.flagEndClosingYmd = flagEndClosingYmd;
    }

    public String getEndClosingYmd() {
        return endClosingYmd;
    }

    public void setEndClosingYmd(String endClosingYmd) {
        this.endClosingYmd = endClosingYmd;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DdTClosCalendar)) {
            return false;
        }
        DdTClosCalendar other = (DdTClosCalendar) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dd.project.DdTClosCalendar[ id=" + id + " ]";
    }
    
}

