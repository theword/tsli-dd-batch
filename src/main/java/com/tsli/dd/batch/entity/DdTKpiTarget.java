/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsli.dd.batch.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author adisa
 */
@Entity
@Table(name = "DD_T_KPI_TARGET", catalog = "", schema = "")
@NamedQueries({
    @NamedQuery(name = "DdTKpiTarget.findAll", query = "SELECT d FROM DdTKpiTarget d"),
    @NamedQuery(name = "DdTKpiTarget.findById", query = "SELECT d FROM DdTKpiTarget d WHERE d.id = :id"),
    @NamedQuery(name = "DdTKpiTarget.findByKpiLevel", query = "SELECT d FROM DdTKpiTarget d WHERE d.kpiLevel = :kpiLevel"),
    @NamedQuery(name = "DdTKpiTarget.findByCode", query = "SELECT d FROM DdTKpiTarget d WHERE d.code = :code"),
    @NamedQuery(name = "DdTKpiTarget.findByKpiName", query = "SELECT d FROM DdTKpiTarget d WHERE d.kpiName = :kpiName"),
    @NamedQuery(name = "DdTKpiTarget.findByProductionYear", query = "SELECT d FROM DdTKpiTarget d WHERE d.productionYear = :productionYear"),
    @NamedQuery(name = "DdTKpiTarget.findByTargetYear", query = "SELECT d FROM DdTKpiTarget d WHERE d.targetYear = :targetYear"),
    @NamedQuery(name = "DdTKpiTarget.findByTargetM1", query = "SELECT d FROM DdTKpiTarget d WHERE d.targetM1 = :targetM1"),
    @NamedQuery(name = "DdTKpiTarget.findByTargetM2", query = "SELECT d FROM DdTKpiTarget d WHERE d.targetM2 = :targetM2"),
    @NamedQuery(name = "DdTKpiTarget.findByTargetM3", query = "SELECT d FROM DdTKpiTarget d WHERE d.targetM3 = :targetM3"),
    @NamedQuery(name = "DdTKpiTarget.findByTargetM4", query = "SELECT d FROM DdTKpiTarget d WHERE d.targetM4 = :targetM4"),
    @NamedQuery(name = "DdTKpiTarget.findByTargetM5", query = "SELECT d FROM DdTKpiTarget d WHERE d.targetM5 = :targetM5"),
    @NamedQuery(name = "DdTKpiTarget.findByTargetM6", query = "SELECT d FROM DdTKpiTarget d WHERE d.targetM6 = :targetM6"),
    @NamedQuery(name = "DdTKpiTarget.findByTargetM7", query = "SELECT d FROM DdTKpiTarget d WHERE d.targetM7 = :targetM7"),
    @NamedQuery(name = "DdTKpiTarget.findByTargetM8", query = "SELECT d FROM DdTKpiTarget d WHERE d.targetM8 = :targetM8"),
    @NamedQuery(name = "DdTKpiTarget.findByTargetM9", query = "SELECT d FROM DdTKpiTarget d WHERE d.targetM9 = :targetM9"),
    @NamedQuery(name = "DdTKpiTarget.findByTargetM10", query = "SELECT d FROM DdTKpiTarget d WHERE d.targetM10 = :targetM10"),
    @NamedQuery(name = "DdTKpiTarget.findByTargetM11", query = "SELECT d FROM DdTKpiTarget d WHERE d.targetM11 = :targetM11"),
    @NamedQuery(name = "DdTKpiTarget.findByTargetM12", query = "SELECT d FROM DdTKpiTarget d WHERE d.targetM12 = :targetM12"),
    @NamedQuery(name = "DdTKpiTarget.findByCreateBy", query = "SELECT d FROM DdTKpiTarget d WHERE d.createBy = :createBy"),
    @NamedQuery(name = "DdTKpiTarget.findByCreateDate", query = "SELECT d FROM DdTKpiTarget d WHERE d.createDate = :createDate"),
    @NamedQuery(name = "DdTKpiTarget.findByUpdateBy", query = "SELECT d FROM DdTKpiTarget d WHERE d.updateBy = :updateBy"),
    @NamedQuery(name = "DdTKpiTarget.findByUpeateDate", query = "SELECT d FROM DdTKpiTarget d WHERE d.upeateDate = :upeateDate")})
public class DdTKpiTarget implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private BigDecimal id;
    @Column(name = "KPI_LEVEL")
    private String kpiLevel;
    @Column(name = "CODE")
    private String code;
    @Column(name = "KPI_NAME")
    private String kpiName;
    @Column(name = "PRODUCTION_YEAR")
    private String productionYear;
    @Column(name = "TARGET_YEAR")
    private BigDecimal targetYear;
    @Column(name = "TARGET_M1")
    private BigDecimal targetM1;
    @Column(name = "TARGET_M2")
    private BigDecimal targetM2;
    @Column(name = "TARGET_M3")
    private BigDecimal targetM3;
    @Column(name = "TARGET_M4")
    private BigDecimal targetM4;
    @Column(name = "TARGET_M5")
    private BigDecimal targetM5;
    @Column(name = "TARGET_M6")
    private BigDecimal targetM6;
    @Column(name = "TARGET_M7")
    private BigDecimal targetM7;
    @Column(name = "TARGET_M8")
    private BigDecimal targetM8;
    @Column(name = "TARGET_M9")
    private BigDecimal targetM9;
    @Column(name = "TARGET_M10")
    private BigDecimal targetM10;
    @Column(name = "TARGET_M11")
    private BigDecimal targetM11;
    @Column(name = "TARGET_M12")
    private BigDecimal targetM12;
    @Column(name = "CREATE_BY")
    private String createBy;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UPDATE_BY")
    private String updateBy;
    @Column(name = "UPEATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date upeateDate;

    public DdTKpiTarget() {
    }

    public DdTKpiTarget(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getKpiLevel() {
        return kpiLevel;
    }

    public void setKpiLevel(String kpiLevel) {
        this.kpiLevel = kpiLevel;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getKpiName() {
        return kpiName;
    }

    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

    public String getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(String productionYear) {
        this.productionYear = productionYear;
    }

    public BigDecimal getTargetYear() {
        return targetYear;
    }

    public void setTargetYear(BigDecimal targetYear) {
        this.targetYear = targetYear;
    }

    public BigDecimal getTargetM1() {
        return targetM1;
    }

    public void setTargetM1(BigDecimal targetM1) {
        this.targetM1 = targetM1;
    }

    public BigDecimal getTargetM2() {
        return targetM2;
    }

    public void setTargetM2(BigDecimal targetM2) {
        this.targetM2 = targetM2;
    }

    public BigDecimal getTargetM3() {
        return targetM3;
    }

    public void setTargetM3(BigDecimal targetM3) {
        this.targetM3 = targetM3;
    }

    public BigDecimal getTargetM4() {
        return targetM4;
    }

    public void setTargetM4(BigDecimal targetM4) {
        this.targetM4 = targetM4;
    }

    public BigDecimal getTargetM5() {
        return targetM5;
    }

    public void setTargetM5(BigDecimal targetM5) {
        this.targetM5 = targetM5;
    }

    public BigDecimal getTargetM6() {
        return targetM6;
    }

    public void setTargetM6(BigDecimal targetM6) {
        this.targetM6 = targetM6;
    }

    public BigDecimal getTargetM7() {
        return targetM7;
    }

    public void setTargetM7(BigDecimal targetM7) {
        this.targetM7 = targetM7;
    }

    public BigDecimal getTargetM8() {
        return targetM8;
    }

    public void setTargetM8(BigDecimal targetM8) {
        this.targetM8 = targetM8;
    }

    public BigDecimal getTargetM9() {
        return targetM9;
    }

    public void setTargetM9(BigDecimal targetM9) {
        this.targetM9 = targetM9;
    }

    public BigDecimal getTargetM10() {
        return targetM10;
    }

    public void setTargetM10(BigDecimal targetM10) {
        this.targetM10 = targetM10;
    }

    public BigDecimal getTargetM11() {
        return targetM11;
    }

    public void setTargetM11(BigDecimal targetM11) {
        this.targetM11 = targetM11;
    }

    public BigDecimal getTargetM12() {
        return targetM12;
    }

    public void setTargetM12(BigDecimal targetM12) {
        this.targetM12 = targetM12;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpeateDate() {
        return upeateDate;
    }

    public void setUpeateDate(Date upeateDate) {
        this.upeateDate = upeateDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DdTKpiTarget)) {
            return false;
        }
        DdTKpiTarget other = (DdTKpiTarget) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
	public String toString() {
		return "DdTKpiTarget [id=" + id + ", kpiLevel=" + kpiLevel + ", code=" + code + ", kpiName=" + kpiName
				+ ", productionYear=" + productionYear + ", targetYear=" + targetYear + ", targetM1=" + targetM1
				+ ", targetM2=" + targetM2 + ", targetM3=" + targetM3 + ", targetM4=" + targetM4 + ", targetM5="
				+ targetM5 + ", targetM6=" + targetM6 + ", targetM7=" + targetM7 + ", targetM8=" + targetM8
				+ ", targetM9=" + targetM9 + ", targetM10=" + targetM10 + ", targetM11=" + targetM11 + ", targetM12="
				+ targetM12 + ", createBy=" + createBy + ", createDate=" + createDate + ", updateBy=" + updateBy
				+ ", upeateDate=" + upeateDate + "]";
	}
    
}

