/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsli.dd.batch.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "DD_T_AGENT_PERFORMANCE", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DdTAgentPerformance.findAll", query = "SELECT d FROM DdTAgentPerformance d")
    , @NamedQuery(name = "DdTAgentPerformance.findById", query = "SELECT d FROM DdTAgentPerformance d WHERE d.id = :id")
    , @NamedQuery(name = "DdTAgentPerformance.findByClosYm", query = "SELECT d FROM DdTAgentPerformance d WHERE d.closYm = :closYm")
    , @NamedQuery(name = "DdTAgentPerformance.findByGroupChannel", query = "SELECT d FROM DdTAgentPerformance d WHERE d.groupChannel = :groupChannel")
    , @NamedQuery(name = "DdTAgentPerformance.findByChannel", query = "SELECT d FROM DdTAgentPerformance d WHERE d.channel = :channel")
    , @NamedQuery(name = "DdTAgentPerformance.findByChannelName", query = "SELECT d FROM DdTAgentPerformance d WHERE d.channelName = :channelName")
    , @NamedQuery(name = "DdTAgentPerformance.findByZone", query = "SELECT d FROM DdTAgentPerformance d WHERE d.zone = :zone")
    , @NamedQuery(name = "DdTAgentPerformance.findByZoneCode", query = "SELECT d FROM DdTAgentPerformance d WHERE d.zoneCode = :zoneCode")
    , @NamedQuery(name = "DdTAgentPerformance.findByZoneDesc", query = "SELECT d FROM DdTAgentPerformance d WHERE d.zoneDesc = :zoneDesc")
    , @NamedQuery(name = "DdTAgentPerformance.findByGmCode", query = "SELECT d FROM DdTAgentPerformance d WHERE d.gmCode = :gmCode")
    , @NamedQuery(name = "DdTAgentPerformance.findByAvpCode", query = "SELECT d FROM DdTAgentPerformance d WHERE d.avpCode = :avpCode")
    , @NamedQuery(name = "DdTAgentPerformance.findByAlCode", query = "SELECT d FROM DdTAgentPerformance d WHERE d.alCode = :alCode")
    , @NamedQuery(name = "DdTAgentPerformance.findByAgentCode", query = "SELECT d FROM DdTAgentPerformance d WHERE d.agentCode = :agentCode")
    , @NamedQuery(name = "DdTAgentPerformance.findByPositionName", query = "SELECT d FROM DdTAgentPerformance d WHERE d.positionName = :positionName")
    , @NamedQuery(name = "DdTAgentPerformance.findByAgentLevel", query = "SELECT d FROM DdTAgentPerformance d WHERE d.agentLevel = :agentLevel")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue1", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue1 = :kpiValue1")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue2", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue2 = :kpiValue2")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue3", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue3 = :kpiValue3")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue4", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue4 = :kpiValue4")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue5", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue5 = :kpiValue5")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue6", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue6 = :kpiValue6")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue7", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue7 = :kpiValue7")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue8", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue8 = :kpiValue8")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue9", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue9 = :kpiValue9")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue10", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue10 = :kpiValue10")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue11", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue11 = :kpiValue11")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue12", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue12 = :kpiValue12")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue13", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue13 = :kpiValue13")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue14", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue14 = :kpiValue14")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue15", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue15 = :kpiValue15")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue16", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue16 = :kpiValue16")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue17", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue17 = :kpiValue17")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue18", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue18 = :kpiValue18")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue19", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue19 = :kpiValue19")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue20", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue20 = :kpiValue20")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue21", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue21 = :kpiValue21")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue22", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue22 = :kpiValue22")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue23", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue23 = :kpiValue23")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue24", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue24 = :kpiValue24")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue25", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue25 = :kpiValue25")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue26", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue26 = :kpiValue26")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue27", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue27 = :kpiValue27")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue28", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue28 = :kpiValue28")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue29", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue29 = :kpiValue29")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue30", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue30 = :kpiValue30")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue31", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue31 = :kpiValue31")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue32", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue32 = :kpiValue32")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue33", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue33 = :kpiValue33")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue34", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue34 = :kpiValue34")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue35", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue35 = :kpiValue35")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue36", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue36 = :kpiValue36")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue37", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue37 = :kpiValue37")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue38", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue38 = :kpiValue38")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue39", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue39 = :kpiValue39")
    , @NamedQuery(name = "DdTAgentPerformance.findByKpiValue40", query = "SELECT d FROM DdTAgentPerformance d WHERE d.kpiValue40 = :kpiValue40")
    , @NamedQuery(name = "DdTAgentPerformance.findByRegDtm", query = "SELECT d FROM DdTAgentPerformance d WHERE d.regDtm = :regDtm")
    , @NamedQuery(name = "DdTAgentPerformance.findByPeNoReg", query = "SELECT d FROM DdTAgentPerformance d WHERE d.peNoReg = :peNoReg")
    , @NamedQuery(name = "DdTAgentPerformance.findByChgDtm", query = "SELECT d FROM DdTAgentPerformance d WHERE d.chgDtm = :chgDtm")
    , @NamedQuery(name = "DdTAgentPerformance.findByPeNoChg", query = "SELECT d FROM DdTAgentPerformance d WHERE d.peNoChg = :peNoChg")
    , @NamedQuery(name = "DdTAgentPerformance.findBySnapFlag", query = "SELECT d FROM DdTAgentPerformance d WHERE d.snapFlag = :snapFlag")
    , @NamedQuery(name = "DdTAgentPerformance.findBySnapDate", query = "SELECT d FROM DdTAgentPerformance d WHERE d.snapDate = :snapDate")})
public class DdTAgentPerformance implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, insertable = true, updatable = true)
    private Integer id;
    @Column(name = "CLOS_YM", length = 18)
    private String closYm;
    @Column(name = "GROUP_CHANNEL", length = 3)
    private String groupChannel;
    @Column(name = "CHANNEL", length = 1)
    private String channel;
    @Column(name = "CHANNEL_NAME", length = 30)
    private String channelName;
    @Column(name = "ZONE", length = 4)
    private String zone;
    @Column(name = "ZONE_CODE", length = 2)
    private String zoneCode;
    @Column(name = "ZONE_DESC", length = 100)
    private String zoneDesc;
    @Column(name = "GM_CODE", length = 30)
    private String gmCode;
    @Column(name = "AVP_CODE", length = 30)
    private String avpCode;
    @Column(name = "AL_CODE", length = 30)
    private String alCode;
    @Column(name = "AGENT_CODE", length = 30)
    private String agentCode;
    @Column(name = "POSITION_NAME", length = 100)
    private String positionName;
    @Column(name = "AGENT_LEVEL", length = 5)
    private String agentLevel;
    @Column(name = "KPI_VALUE_1")
    private BigDecimal kpiValue1;
    @Column(name = "KPI_VALUE_2")
    private BigDecimal kpiValue2;
    @Column(name = "KPI_VALUE_3")
    private BigDecimal kpiValue3;
    @Column(name = "KPI_VALUE_4")
    private BigDecimal kpiValue4;
    @Column(name = "KPI_VALUE_5")
    private BigDecimal kpiValue5;
    @Column(name = "KPI_VALUE_6")
    private BigDecimal kpiValue6;
    @Column(name = "KPI_VALUE_7")
    private BigDecimal kpiValue7;
    @Column(name = "KPI_VALUE_8")
    private BigDecimal kpiValue8;
    @Column(name = "KPI_VALUE_9")
    private BigDecimal kpiValue9;
    @Column(name = "KPI_VALUE_10")
    private BigDecimal kpiValue10;
    @Column(name = "KPI_VALUE_11")
    private BigDecimal kpiValue11;
    @Column(name = "KPI_VALUE_12")
    private BigDecimal kpiValue12;
    @Column(name = "KPI_VALUE_13")
    private BigDecimal kpiValue13;
    @Column(name = "KPI_VALUE_14")
    private BigDecimal kpiValue14;
    @Column(name = "KPI_VALUE_15")
    private BigDecimal kpiValue15;
    @Column(name = "KPI_VALUE_16")
    private BigDecimal kpiValue16;
    @Column(name = "KPI_VALUE_17")
    private BigDecimal kpiValue17;
    @Column(name = "KPI_VALUE_18")
    private BigDecimal kpiValue18;
    @Column(name = "KPI_VALUE_19")
    private BigDecimal kpiValue19;
    @Column(name = "KPI_VALUE_20")
    private BigDecimal kpiValue20;
    @Column(name = "KPI_VALUE_21")
    private BigDecimal kpiValue21;
    @Column(name = "KPI_VALUE_22")
    private BigDecimal kpiValue22;
    @Column(name = "KPI_VALUE_23")
    private BigDecimal kpiValue23;
    @Column(name = "KPI_VALUE_24")
    private BigDecimal kpiValue24;
    @Column(name = "KPI_VALUE_25")
    private BigDecimal kpiValue25;
    @Column(name = "KPI_VALUE_26")
    private BigDecimal kpiValue26;
    @Column(name = "KPI_VALUE_27")
    private BigDecimal kpiValue27;
    @Column(name = "KPI_VALUE_28")
    private BigDecimal kpiValue28;
    @Column(name = "KPI_VALUE_29")
    private BigDecimal kpiValue29;
    @Column(name = "KPI_VALUE_30")
    private BigDecimal kpiValue30;
    @Column(name = "KPI_VALUE_31")
    private BigDecimal kpiValue31;
    @Column(name = "KPI_VALUE_32")
    private BigDecimal kpiValue32;
    @Column(name = "KPI_VALUE_33")
    private BigDecimal kpiValue33;
    @Column(name = "KPI_VALUE_34")
    private BigDecimal kpiValue34;
    @Column(name = "KPI_VALUE_35")
    private BigDecimal kpiValue35;
    @Column(name = "KPI_VALUE_36")
    private BigDecimal kpiValue36;
    @Column(name = "KPI_VALUE_37")
    private BigDecimal kpiValue37;
    @Column(name = "KPI_VALUE_38")
    private BigDecimal kpiValue38;
    @Column(name = "KPI_VALUE_39")
    private BigDecimal kpiValue39;
    @Column(name = "KPI_VALUE_40")
    private BigDecimal kpiValue40;
    @Column(name = "REG_DTM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date regDtm;
    @Column(name = "PE_NO_REG", length = 2)
    private String peNoReg;
    @Column(name = "CHG_DTM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date chgDtm;
    @Column(name = "PE_NO_CHG", length = 9)
    private String peNoChg;
    @Column(name = "SNAP_FLAG")
    private String snapFlag;
    @Column(name = "SNAP_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date snapDate;

    public DdTAgentPerformance() {
    }

    public DdTAgentPerformance(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClosYm() {
        return closYm;
    }

    public void setClosYm(String closYm) {
        this.closYm = closYm;
    }

    public String getGroupChannel() {
        return groupChannel;
    }

    public void setGroupChannel(String groupChannel) {
        this.groupChannel = groupChannel;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getZoneCode() {
        return zoneCode;
    }

    public void setZoneCode(String zoneCode) {
        this.zoneCode = zoneCode;
    }

    public String getZoneDesc() {
        return zoneDesc;
    }

    public void setZoneDesc(String zoneDesc) {
        this.zoneDesc = zoneDesc;
    }

    public String getGmCode() {
        return gmCode;
    }

    public void setGmCode(String gmCode) {
        this.gmCode = gmCode;
    }

    public String getAvpCode() {
        return avpCode;
    }

    public void setAvpCode(String avpCode) {
        this.avpCode = avpCode;
    }

    public String getAlCode() {
        return alCode;
    }

    public void setAlCode(String alCode) {
        this.alCode = alCode;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getAgentLevel() {
        return agentLevel;
    }

    public void setAgentLevel(String agentLevel) {
        this.agentLevel = agentLevel;
    }

    public BigDecimal getKpiValue1() {
        return kpiValue1;
    }

    public void setKpiValue1(BigDecimal kpiValue1) {
        this.kpiValue1 = kpiValue1;
    }

    public BigDecimal getKpiValue2() {
        return kpiValue2;
    }

    public void setKpiValue2(BigDecimal kpiValue2) {
        this.kpiValue2 = kpiValue2;
    }

    public BigDecimal getKpiValue3() {
        return kpiValue3;
    }

    public void setKpiValue3(BigDecimal kpiValue3) {
        this.kpiValue3 = kpiValue3;
    }

    public BigDecimal getKpiValue4() {
        return kpiValue4;
    }

    public void setKpiValue4(BigDecimal kpiValue4) {
        this.kpiValue4 = kpiValue4;
    }

    public BigDecimal getKpiValue5() {
        return kpiValue5;
    }

    public void setKpiValue5(BigDecimal kpiValue5) {
        this.kpiValue5 = kpiValue5;
    }

    public BigDecimal getKpiValue6() {
        return kpiValue6;
    }

    public void setKpiValue6(BigDecimal kpiValue6) {
        this.kpiValue6 = kpiValue6;
    }

    public BigDecimal getKpiValue7() {
        return kpiValue7;
    }

    public void setKpiValue7(BigDecimal kpiValue7) {
        this.kpiValue7 = kpiValue7;
    }

    public BigDecimal getKpiValue8() {
        return kpiValue8;
    }

    public void setKpiValue8(BigDecimal kpiValue8) {
        this.kpiValue8 = kpiValue8;
    }

    public BigDecimal getKpiValue9() {
        return kpiValue9;
    }

    public void setKpiValue9(BigDecimal kpiValue9) {
        this.kpiValue9 = kpiValue9;
    }

    public BigDecimal getKpiValue10() {
        return kpiValue10;
    }

    public void setKpiValue10(BigDecimal kpiValue10) {
        this.kpiValue10 = kpiValue10;
    }

    public BigDecimal getKpiValue11() {
        return kpiValue11;
    }

    public void setKpiValue11(BigDecimal kpiValue11) {
        this.kpiValue11 = kpiValue11;
    }

    public BigDecimal getKpiValue12() {
        return kpiValue12;
    }

    public void setKpiValue12(BigDecimal kpiValue12) {
        this.kpiValue12 = kpiValue12;
    }

    public BigDecimal getKpiValue13() {
        return kpiValue13;
    }

    public void setKpiValue13(BigDecimal kpiValue13) {
        this.kpiValue13 = kpiValue13;
    }

    public BigDecimal getKpiValue14() {
        return kpiValue14;
    }

    public void setKpiValue14(BigDecimal kpiValue14) {
        this.kpiValue14 = kpiValue14;
    }

    public BigDecimal getKpiValue15() {
        return kpiValue15;
    }

    public void setKpiValue15(BigDecimal kpiValue15) {
        this.kpiValue15 = kpiValue15;
    }

    public BigDecimal getKpiValue16() {
        return kpiValue16;
    }

    public void setKpiValue16(BigDecimal kpiValue16) {
        this.kpiValue16 = kpiValue16;
    }

    public BigDecimal getKpiValue17() {
        return kpiValue17;
    }

    public void setKpiValue17(BigDecimal kpiValue17) {
        this.kpiValue17 = kpiValue17;
    }

    public BigDecimal getKpiValue18() {
        return kpiValue18;
    }

    public void setKpiValue18(BigDecimal kpiValue18) {
        this.kpiValue18 = kpiValue18;
    }

    public BigDecimal getKpiValue19() {
        return kpiValue19;
    }

    public void setKpiValue19(BigDecimal kpiValue19) {
        this.kpiValue19 = kpiValue19;
    }

    public BigDecimal getKpiValue20() {
        return kpiValue20;
    }

    public void setKpiValue20(BigDecimal kpiValue20) {
        this.kpiValue20 = kpiValue20;
    }

    public BigDecimal getKpiValue21() {
        return kpiValue21;
    }

    public void setKpiValue21(BigDecimal kpiValue21) {
        this.kpiValue21 = kpiValue21;
    }

    public BigDecimal getKpiValue22() {
        return kpiValue22;
    }

    public void setKpiValue22(BigDecimal kpiValue22) {
        this.kpiValue22 = kpiValue22;
    }

    public BigDecimal getKpiValue23() {
        return kpiValue23;
    }

    public void setKpiValue23(BigDecimal kpiValue23) {
        this.kpiValue23 = kpiValue23;
    }

    public BigDecimal getKpiValue24() {
        return kpiValue24;
    }

    public void setKpiValue24(BigDecimal kpiValue24) {
        this.kpiValue24 = kpiValue24;
    }

    public BigDecimal getKpiValue25() {
        return kpiValue25;
    }

    public void setKpiValue25(BigDecimal kpiValue25) {
        this.kpiValue25 = kpiValue25;
    }

    public BigDecimal getKpiValue26() {
        return kpiValue26;
    }

    public void setKpiValue26(BigDecimal kpiValue26) {
        this.kpiValue26 = kpiValue26;
    }

    public BigDecimal getKpiValue27() {
        return kpiValue27;
    }

    public void setKpiValue27(BigDecimal kpiValue27) {
        this.kpiValue27 = kpiValue27;
    }

    public BigDecimal getKpiValue28() {
        return kpiValue28;
    }

    public void setKpiValue28(BigDecimal kpiValue28) {
        this.kpiValue28 = kpiValue28;
    }

    public BigDecimal getKpiValue29() {
        return kpiValue29;
    }

    public void setKpiValue29(BigDecimal kpiValue29) {
        this.kpiValue29 = kpiValue29;
    }

    public BigDecimal getKpiValue30() {
        return kpiValue30;
    }

    public void setKpiValue30(BigDecimal kpiValue30) {
        this.kpiValue30 = kpiValue30;
    }

    public BigDecimal getKpiValue31() {
        return kpiValue31;
    }

    public void setKpiValue31(BigDecimal kpiValue31) {
        this.kpiValue31 = kpiValue31;
    }

    public BigDecimal getKpiValue32() {
        return kpiValue32;
    }

    public void setKpiValue32(BigDecimal kpiValue32) {
        this.kpiValue32 = kpiValue32;
    }

    public BigDecimal getKpiValue33() {
        return kpiValue33;
    }

    public void setKpiValue33(BigDecimal kpiValue33) {
        this.kpiValue33 = kpiValue33;
    }

    public BigDecimal getKpiValue34() {
        return kpiValue34;
    }

    public void setKpiValue34(BigDecimal kpiValue34) {
        this.kpiValue34 = kpiValue34;
    }

    public BigDecimal getKpiValue35() {
        return kpiValue35;
    }

    public void setKpiValue35(BigDecimal kpiValue35) {
        this.kpiValue35 = kpiValue35;
    }

    public BigDecimal getKpiValue36() {
        return kpiValue36;
    }

    public void setKpiValue36(BigDecimal kpiValue36) {
        this.kpiValue36 = kpiValue36;
    }

    public BigDecimal getKpiValue37() {
        return kpiValue37;
    }

    public void setKpiValue37(BigDecimal kpiValue37) {
        this.kpiValue37 = kpiValue37;
    }

    public BigDecimal getKpiValue38() {
        return kpiValue38;
    }

    public void setKpiValue38(BigDecimal kpiValue38) {
        this.kpiValue38 = kpiValue38;
    }

    public BigDecimal getKpiValue39() {
        return kpiValue39;
    }

    public void setKpiValue39(BigDecimal kpiValue39) {
        this.kpiValue39 = kpiValue39;
    }

    public BigDecimal getKpiValue40() {
        return kpiValue40;
    }

    public void setKpiValue40(BigDecimal kpiValue40) {
        this.kpiValue40 = kpiValue40;
    }

    public Date getRegDtm() {
        return regDtm;
    }

    public void setRegDtm(Date regDtm) {
        this.regDtm = regDtm;
    }

    public String getPeNoReg() {
        return peNoReg;
    }

    public void setPeNoReg(String peNoReg) {
        this.peNoReg = peNoReg;
    }

    public Date getChgDtm() {
        return chgDtm;
    }

    public void setChgDtm(Date chgDtm) {
        this.chgDtm = chgDtm;
    }

    public String getPeNoChg() {
        return peNoChg;
    }

    public void setPeNoChg(String peNoChg) {
        this.peNoChg = peNoChg;
    }

    public String getSnapFlag() {
        return snapFlag;
    }

    public void setSnapFlag(String snapFlag) {
        this.snapFlag = snapFlag;
    }

    public Date getSnapDate() {
        return snapDate;
    }

    public void setSnapDate(Date snapDate) {
        this.snapDate = snapDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DdTAgentPerformance)) {
            return false;
        }
        DdTAgentPerformance other = (DdTAgentPerformance) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "DdTAgentPerformance [id=" + id + ", closYm=" + closYm + ", groupChannel=" + groupChannel + ", channel="
				+ channel + ", channelName=" + channelName + ", zone=" + zone + ", zoneCode=" + zoneCode + ", zoneDesc="
				+ zoneDesc + ", gmCode=" + gmCode + ", avpCode=" + avpCode + ", alCode=" + alCode + ", agentCode="
				+ agentCode + ", positionName=" + positionName + ", agentLevel=" + agentLevel + ", kpiValue1="
				+ kpiValue1 + ", kpiValue2=" + kpiValue2 + ", kpiValue3=" + kpiValue3 + ", kpiValue4=" + kpiValue4
				+ ", kpiValue5=" + kpiValue5 + ", kpiValue6=" + kpiValue6 + ", kpiValue7=" + kpiValue7 + ", kpiValue8="
				+ kpiValue8 + ", kpiValue9=" + kpiValue9 + ", kpiValue10=" + kpiValue10 + ", kpiValue11=" + kpiValue11
				+ ", kpiValue12=" + kpiValue12 + ", kpiValue13=" + kpiValue13 + ", kpiValue14=" + kpiValue14
				+ ", kpiValue15=" + kpiValue15 + ", kpiValue16=" + kpiValue16 + ", kpiValue17=" + kpiValue17
				+ ", kpiValue18=" + kpiValue18 + ", kpiValue19=" + kpiValue19 + ", kpiValue20=" + kpiValue20
				+ ", kpiValue21=" + kpiValue21 + ", kpiValue22=" + kpiValue22 + ", kpiValue23=" + kpiValue23
				+ ", kpiValue24=" + kpiValue24 + ", kpiValue25=" + kpiValue25 + ", kpiValue26=" + kpiValue26
				+ ", kpiValue27=" + kpiValue27 + ", kpiValue28=" + kpiValue28 + ", kpiValue29=" + kpiValue29
				+ ", kpiValue30=" + kpiValue30 + ", kpiValue31=" + kpiValue31 + ", kpiValue32=" + kpiValue32
				+ ", kpiValue33=" + kpiValue33 + ", kpiValue34=" + kpiValue34 + ", kpiValue35=" + kpiValue35
				+ ", kpiValue36=" + kpiValue36 + ", kpiValue37=" + kpiValue37 + ", kpiValue38=" + kpiValue38
				+ ", kpiValue39=" + kpiValue39 + ", kpiValue40=" + kpiValue40 + ", regDtm=" + regDtm + ", peNoReg="
				+ peNoReg + ", chgDtm=" + chgDtm + ", peNoChg=" + peNoChg + ", snapFlag=" + snapFlag + ", snapDate="
				+ snapDate + "]";
	}


    
}
