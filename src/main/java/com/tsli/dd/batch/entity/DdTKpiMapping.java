/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsli.dd.batch.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author adisa
 */
@Entity
@Table(name = "DD_T_KPI_MAPPING", catalog = "", schema = "")
@NamedQueries({
    @NamedQuery(name = "DdTKpiMapping.findAll", query = "SELECT d FROM DdTKpiMapping d"),
    @NamedQuery(name = "DdTKpiMapping.findById", query = "SELECT d FROM DdTKpiMapping d WHERE d.id = :id"),
    @NamedQuery(name = "DdTKpiMapping.findByKpiFeild", query = "SELECT d FROM DdTKpiMapping d WHERE d.kpiFeild = :kpiFeild"),
    @NamedQuery(name = "DdTKpiMapping.findByKpiName", query = "SELECT d FROM DdTKpiMapping d WHERE d.kpiName = :kpiName"),
    @NamedQuery(name = "DdTKpiMapping.findByKpiCode", query = "SELECT d FROM DdTKpiMapping d WHERE d.kpiCode = :kpiCode"),
    @NamedQuery(name = "DdTKpiMapping.findByKpiActiveStatus", query = "SELECT d FROM DdTKpiMapping d WHERE d.kpiActiveStatus = :kpiActiveStatus"),
    @NamedQuery(name = "DdTKpiMapping.findByOrderring", query = "SELECT d FROM DdTKpiMapping d WHERE d.orderring = :orderring"),
    @NamedQuery(name = "DdTKpiMapping.findByCreateBy", query = "SELECT d FROM DdTKpiMapping d WHERE d.createBy = :createBy"),
    @NamedQuery(name = "DdTKpiMapping.findByCreateDate", query = "SELECT d FROM DdTKpiMapping d WHERE d.createDate = :createDate"),
    @NamedQuery(name = "DdTKpiMapping.findByUpdateBy", query = "SELECT d FROM DdTKpiMapping d WHERE d.updateBy = :updateBy"),
    @NamedQuery(name = "DdTKpiMapping.findByUpeateDate", query = "SELECT d FROM DdTKpiMapping d WHERE d.upeateDate = :upeateDate")})
public class DdTKpiMapping implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private BigDecimal id;
    @Column(name = "KPI_FEILD")
    private String kpiFeild;
    @Column(name = "KPI_NAME")
    private String kpiName;
    @Column(name = "KPI_CODE")
    private String kpiCode;
    @Column(name = "KPI_ACTIVE_STATUS")
    private String kpiActiveStatus;
    @Column(name = "ORDERRING")
    private BigInteger orderring;
    @Column(name = "CREATE_BY")
    private String createBy;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UPDATE_BY")
    private String updateBy;
    @Column(name = "UPEATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date upeateDate;

    public DdTKpiMapping() {
    }

    public DdTKpiMapping(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getKpiFeild() {
        return kpiFeild;
    }

    public void setKpiFeild(String kpiFeild) {
        this.kpiFeild = kpiFeild;
    }

    public String getKpiName() {
        return kpiName;
    }

    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

    public String getKpiCode() {
        return kpiCode;
    }

    public void setKpiCode(String kpiCode) {
        this.kpiCode = kpiCode;
    }

    public String getKpiActiveStatus() {
        return kpiActiveStatus;
    }

    public void setKpiActiveStatus(String kpiActiveStatus) {
        this.kpiActiveStatus = kpiActiveStatus;
    }

    public BigInteger getOrderring() {
        return orderring;
    }

    public void setOrderring(BigInteger orderring) {
        this.orderring = orderring;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpeateDate() {
        return upeateDate;
    }

    public void setUpeateDate(Date upeateDate) {
        this.upeateDate = upeateDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DdTKpiMapping)) {
            return false;
        }
        DdTKpiMapping other = (DdTKpiMapping) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
	public String toString() {
		return "DdTKpiMapping [id=" + id + ", kpiFeild=" + kpiFeild + ", kpiName=" + kpiName + ", kpiCode=" + kpiCode
				+ ", kpiActiveStatus=" + kpiActiveStatus + ", orderring=" + orderring + ", createBy=" + createBy
				+ ", createDate=" + createDate + ", updateBy=" + updateBy + ", upeateDate=" + upeateDate + "]";
	}
    
}

    